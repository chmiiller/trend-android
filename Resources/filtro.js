function filtro() {

	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var brieFont = 'Brie Light';
	var yanoneBold = 'Yanone Kaffeesatz';

	var dicionarioJs = require("DictionaryCidades");

	var filtroNome = '';
	var filtroRede = '';
	var filtroTipo = 'Nacional';
	var filtroPais = 'Brasil';
	var filtroEstado = '';
	var filtroCidade = '';
	var filtroCatQuarto = '';
	var filtroTipoQuarto = '';
	var filtroEstrelas = '0';
	var filtroPreco = 1;

	var win = Titanium.UI.createWindow({
		backgroundColor:'#2791ca',
		navBarHidden: true,
		fullscreen:true,
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	//
	//LOADING VIEW
	//
	var activityIndicatorContainer = Ti.UI.createView({backgroundColor:'transparent'});
	var bgActivityIndicator = Ti.UI.createView({backgroundColor:'black',opacity:0.7});
	var activityIndicator = Ti.UI.createActivityIndicator({
	  color: 'white',
	  font: {fontFamily:yanoneBold, fontSize:22*res},
	  message: 'Buscando...',
	  style:Ti.UI.ActivityIndicatorStyle.BIG,
	});
	activityIndicator.show();
	activityIndicatorContainer.add(bgActivityIndicator);
	activityIndicatorContainer.add(activityIndicator);
	activityIndicatorContainer.visible = false;


	//
	//PEGAR TODOS OS PAÍSES
	//
	var filePaises = null;
	var blobPaises = null;
	var dataPaises = null;
	var arrayPaises = null;
	filePaises = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "country.json");
	if(filePaises.exists()){
		blobPaises = filePaises.read();
		if(blobPaises){
			arrayPaises = JSON.parse(blobPaises.text);
		}
	}

	var bgView = Titanium.UI.createView({
		backgroundColor:'#2791ca',opacity:0.9,left:0,top:0,width:Ti.UI.FILL,height:Ti.UI.FILL
	});
	win.add(bgView);

	var scroll = Titanium.UI.createScrollView({
		top:0,left:0,width:320*res,backgroundColor:'transparent',layout:'vertical',scrollType:'vertical'
	});

	var filtrarPor = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/filtrar.png',top:10*rh,left:10*rw,width:93*res,height:24*res
	});
	scroll.add(filtrarPor);

	var dadosDoEstabelecimento = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/dados.png',top:15*rh,left:0,width:320*res,height:49*res
	});
	scroll.add(dadosDoEstabelecimento);

	var btFechar = Titanium.UI.createButton({
		backgroundImage:'images/precos/btFechar.png',top:20*rh,right:30*rw,width:26*res,height:26*res
	});
	//scroll.add(btFechar);
	btFechar.addEventListener('click',function(e){
		lbNome.blur();
		lbRede.blur();
		win.close();
	});

	var nomeContainer = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/textfield1.png',top:10*rh,width:296*res,height:60*res
	});
	scroll.add(nomeContainer);

	var lbNome = Ti.UI.createTextField({
		width:250*res,height:57*res,
		bubbleParent:false,color:'white',value:'Nome',
		font:{fontSize:20*res,},hintText:'Nome',
		clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS,
	});
	nomeContainer.add(lbNome);

	lbNome.addEventListener('focus',function(e){
		if(lbNome.value == 'Nome'){
			lbNome.value = '';
		}
	});
	lbNome.addEventListener('return',function(e){
		if(lbNome.value == '' || lbNome.value == ' '){
			lbNome.value = 'Nome';
			filtroNome = '';
			return;
		}
		filtroNome = lbNome.value;
		lbNome.blur();
	});

	lbNome.addEventListener('change',function(e){
		filtroNome = lbNome.value;
	});

	lbNome.addEventListener('blur',function(e){
		if(lbNome.value == '' || lbNome.value == ' '){
			lbNome.value = 'Nome';
			filtroNome = '';
			return;
		}
		filtroNome = lbNome.value;
		lbNome.blur();
	});

	var redeContainer = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/textfield1.png',top:10*rh,width:296*res,height:60*res
	});
	scroll.add(redeContainer);

	var lbRede = Ti.UI.createTextField({
		width:250*res,height:57*res,
		bubbleParent:false,color:'white',value:'Rede',
		font:{fontSize:20*res,},hintText:'Rede',
		clearButtonMode:Titanium.UI.INPUT_BUTTONMODE_ONFOCUS
	});
	redeContainer.add(lbRede);

	lbRede.addEventListener('focus',function(e){
		if(lbRede.value == 'Rede'){
			lbRede.value = '';
		}
	});
	lbRede.addEventListener('return',function(e){
		if(lbRede.value == '' || lbRede.value == ' '){
			lbRede.value = 'Rede';
			filtroRede = '';
		}
		filtroRede = lbRede.value;
		lbRede.blur();
	});

	lbRede.addEventListener('change',function(e){
		filtroRede = lbRede.value;
	});

	scroll.addEventListener('click',function(e){
		lbNome.blur();
		lbRede.blur();
		filtroNome = lbNome.value;
		filtroRede = lbRede.value;
	});

	var tipoPaisContainer = Titanium.UI.createView({
		top:10*rh,left:0,width:320*res,height:65*res,
	});
	scroll.add(tipoPaisContainer);

	var tipoContainer = Titanium.UI.createView({
		left:8*rw,width:145*res,height:60*res,
		backgroundImage:'/images/filtro1/fieldCatQuarto.png'
	});
	tipoPaisContainer.add(tipoContainer);

	var lbTipo = Ti.UI.createLabel({
		width:90*res,height:55*res,left:10*res,
		bubbleParent:false,color:'white',text:'Nacional',
		font:{fontSize:17*res,},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
	});
	tipoContainer.add(lbTipo);
	lbTipo.addEventListener('click',createOptionTipo);

	function createOptionTipo(){
		var dialog = Ti.UI.createOptionDialog({
			title:'Categoria do Quarto:',
			options: ['Nacional','Internacional','Lazer','Eventos','Cancelar'],
			cancel: 4,
		});
		dialog.show();
		dialog.addEventListener('click',function(e){
			if(e.index != 4){
				//Se for INTERNACIONAL, diminuir o tamanho da fonte
				if(e.index == 1){
					lbTipo.setFont({fontSize:14*res});
					estadoContainer.setOpacity(0.5);
					cidadeContainer.setOpacity(0.5);
				}else{
					lbTipo.setFont({fontSize:17*res});
					filtroPais = "Brasil";
					estadoContainer.setOpacity(1);
					cidadeContainer.setOpacity(1);
				}
				var result = '' + e.source.options[e.index];
				filtroTipo = result;
				lbTipo.setText(result);
			}
		});
	}

	//
	//Picker de países
	//
	var paisContainer = Titanium.UI.createView({
		right:8*rw,width:145*res,height:60*res,
		backgroundImage:'/images/filtro1/fieldCatQuarto.png'
	});
	tipoPaisContainer.add(paisContainer);

	//PICKER DE TIPO (corporativo, internacional, lazer, eventos)
	var pickerPais = Ti.UI.createPicker({font:{fontSize:30*res},left:7*res});

	pickerPais.addEventListener('change', function(e) {
		var resultPickerTitle = pickerPais.getSelectedRow(0).title;
		filtroPais = resultPickerTitle;
		if(filtroPais == "Brasil"){
			lbTipo.setFont({fontSize:17*res});
			lbTipo.setText('Nacional');
			filtroTipo = 'Nacional';
		}else{
			lbTipo.setFont({fontSize:14*res});
			lbTipo.setText('Internacional');
			filtroTipo = 'Internacional';
		}
	});

	var dataPickerPais = [];
	for(var k = 0; k < arrayPaises.length; k++){
		var row = Ti.UI.createPickerRow({title:arrayPaises[k]})
		dataPickerPais.push(row);
	}

	pickerPais.add(dataPickerPais);
	paisContainer.add(pickerPais);

	var regiaoContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:10*rh,left:0,width:320*res,height:70*res
	});
	scroll.add(regiaoContainer);

	var estadoContainer = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/fieldEstado.png',left:11*rw,width:106.5*res,height:60*res
	});
	regiaoContainer.add(estadoContainer);

	var cidadeContainer = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/fieldCidade.png',right:12*rw,width:186*res,height:60*res
	});
	regiaoContainer.add(cidadeContainer);

	setTimeout(function(e){
		lbNome.blur();
		lbRede.blur();
		lbNome.setValue('Nome');
	},200);
	//PICKER DE ESTADOS
	var pickerEstados = Ti.UI.createPicker({font:{fontSize:30*res}});

	pickerEstados.addEventListener('change', function(e) {
		var resultPickerValue = pickerEstados.getSelectedRow(0).valor;
		var resultPickerTitle = pickerEstados.getSelectedRow(0).title;
		filtroEstado = resultPickerValue;
		refreshPickerCidades(resultPickerTitle);

		if(filtroTipo == 'Internacional'){
			lbTipo.setFont({fontSize:17*res});
			lbTipo.setText('Nacional');
			filtroTipo = 'Nacional';
		}
		filtroPais = "Brasil";
		estadoContainer.setOpacity(1);
		cidadeContainer.setOpacity(1);
	});

	var data = 
	[
		Ti.UI.createPickerRow({title:'-',valor:'-'}),
		Ti.UI.createPickerRow({title:'AC',valor:'Acre'}),Ti.UI.createPickerRow({title:'AL',valor:'Alagoas'}),
		Ti.UI.createPickerRow({title:'AP',valor:'Amapá'}),Ti.UI.createPickerRow({title:'AM',valor:'Amazonas'}),
		Ti.UI.createPickerRow({title:'BA',valor:'Bahia'}),Ti.UI.createPickerRow({title:'CE',valor:'Ceará'}),
		Ti.UI.createPickerRow({title:'DF',valor:'Distrito Federal'}),Ti.UI.createPickerRow({title:'ES',valor:'Espírito Santo'}),
		Ti.UI.createPickerRow({title:'GO',valor:'Goiás'}),Ti.UI.createPickerRow({title:'MA',valor:'Maranhão'}),
		Ti.UI.createPickerRow({title:'MT',valor:'Mato Grosso'}),Ti.UI.createPickerRow({title:'MS',valor:'Mato Grosso do Sul'}),
		Ti.UI.createPickerRow({title:'MG',valor:'Minas Gerais'}),Ti.UI.createPickerRow({title:'PR',valor:'Paraná'}),
		Ti.UI.createPickerRow({title:'PB',valor:'Paraiba'}),Ti.UI.createPickerRow({title:'PA',valor:'Pará'}),
		Ti.UI.createPickerRow({title:'PE',valor:'Pernambuco'}),Ti.UI.createPickerRow({title:'PI',valor:'Piauí'}),
		Ti.UI.createPickerRow({title:'RJ',valor:'Rio de Janeiro'}),Ti.UI.createPickerRow({title:'RN',valor:'Rio Grande do Norte'}),
		Ti.UI.createPickerRow({title:'RS',valor:'Rio Grande do Sul'}),Ti.UI.createPickerRow({title:'RO',valor:'Rondônia'}),
		Ti.UI.createPickerRow({title:'RR',valor:'Roraima'}),Ti.UI.createPickerRow({title:'SC',valor:'Santa Catarina'}),
		Ti.UI.createPickerRow({title:'SE',valor:'Sergipe'}),Ti.UI.createPickerRow({title:'SP',valor:'São Paulo'}),
		Ti.UI.createPickerRow({title:'TO',valor:'Tocantins'})
	];

	pickerEstados.add(data);
	estadoContainer.add(pickerEstados);

	var pickerCidades = Ti.UI.createPicker({font:{fontSize:30*res}});
	refreshPickerCidades('-');
	
	function pickerCidadesHandler(e){
		var resultPickerValueCidades = pickerCidades.getSelectedRow(0).title;
		filtroCidade = resultPickerValueCidades;

		lbTipo.setFont({fontSize:17*res});
		lbTipo.setText('Nacional');
		filtroTipo = 'Nacional';
		filtroPais = "Brasil";
		estadoContainer.setOpacity(1);
		cidadeContainer.setOpacity(1);
	}

	function refreshPickerCidades(estado){
		if(estado != '-'){
			var cidades = dicionarioJs(estado);
			var novaData = [];
			cidadeContainer.remove(pickerCidades);
			pickerCidades.removeEventListener('change', pickerCidadesHandler);
			pickerCidades = null;
			pickerCidades = Ti.UI.createPicker({font:{fontSize:30*res}});
			for(var i = 0; i < cidades.length; i++){
				var row = Ti.UI.createPickerRow({title:cidades[i],valor:cidades[i]});
				novaData.push(row);
			}
			filtroCidade = cidades[0];
			pickerCidades.add(novaData);
			cidadeContainer.add(pickerCidades);
			pickerCidades.addEventListener('change', pickerCidadesHandler);
		}else{
			var novaData = [];
			cidadeContainer.remove(pickerCidades);
			pickerCidades.removeEventListener('change', pickerCidadesHandler);
			pickerCidades = null;
			pickerCidades = Ti.UI.createPicker({font:{fontSize:30*res}});
			var row = Ti.UI.createPickerRow({title:'-',valor:'-'});
			novaData.push(row);
			filtroCidade = '-';
			pickerCidades.add(novaData);
			cidadeContainer.add(pickerCidades);
			pickerCidades.addEventListener('change', pickerCidadesHandler);
		}
		
	}

	cidadeContainer.add(pickerCidades);

	//QUARTOS
	var titulosContainer = Titanium.UI.createView({
		top:15*rh,left:0,width:320*res,height:20*res
	});
	scroll.add(titulosContainer);

	var catQuarto = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/catQuarto.png',left:20*rw,width:95*res,height:12*res
	});
	titulosContainer.add(catQuarto);

	var tipoQuarto = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/tipoQuarto.png',right:30*rw,width:96*res,height:12*res
	});
	titulosContainer.add(tipoQuarto);

	var quartoContainer = Titanium.UI.createView({
		top:3*rh,left:0,width:320*res,height:65*res,
	});
	scroll.add(quartoContainer);

	var catQuartosContainer = Titanium.UI.createView({
		left:8*rw,width:145*res,height:60*res,
		backgroundImage:'/images/filtro1/fieldCatQuarto.png'
	});
	quartoContainer.add(catQuartosContainer);

	var lbCatQuartos = Ti.UI.createLabel({
		width:90*res,height:55*res,
		bubbleParent:false,color:'white',text:'-',
		font:{fontSize:18*res,},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
	});
	catQuartosContainer.add(lbCatQuartos);
	filtroCatQuarto = '';
	lbCatQuartos.addEventListener('click',createOptionCategoria);

	var tipoQuartosContainer = Titanium.UI.createView({
		right:8*rw,width:145*res,height:60*res,
		backgroundImage:'/images/filtro1/fieldCatQuarto.png'
	});
	quartoContainer.add(tipoQuartosContainer);

	var lbTipoQuartos = Ti.UI.createLabel({
		width:90*res,height:55*res,
		bubbleParent:false,color:'white',text:'-',
		font:{fontSize:19*res,},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
	});
	tipoQuartosContainer.add(lbTipoQuartos);
	filtroTipoQuarto = '';
	lbTipoQuartos.addEventListener('click',createOptionTipoQuarto);

	function createOptionCategoria(){
		var dialog = Ti.UI.createOptionDialog({
			title:'Categoria do Quarto:',
			options: ['FLAT','Cat1','Cancelar'],
			cancel: 2,
		});
		dialog.show();
		dialog.addEventListener('click',function(e){
			if(e.index != 2){
				var result = '' + e.source.options[e.index];
				filtroCatQuarto = result;
				lbCatQuartos.setText(result);
			}
		});
	}

	function createOptionTipoQuarto(){
		var dialog = Ti.UI.createOptionDialog({
			title:'Categoria do Quarto:',
			options: ['SGL','DBL','Cancelar'],
			cancel: 2,
		});
		dialog.show();
		dialog.addEventListener('click',function(e){
			if(e.index != 2){
				var result = '' + e.source.options[e.index];
				filtroTipoQuarto = result;
				lbTipoQuartos.setText(result);
			}
		});
	}

	//ESTRELAS
	var tituloQtEstrelas = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/qtdEstrelas.png',
		top:20*rh,left:9*rw,width:176*res,height:14*res,
	});
	scroll.add(tituloQtEstrelas);

	var starsContainer = Ti.UI.createView({
		top:20*rh,left:9*rw,width:255*res,height:50*res,layout:'horizontal'
	});
	scroll.add(starsContainer);

	var estrela1 = Ti.UI.createView({
		backgroundImage:'images/filtro1/estrela.png',width:44.5*res,height:42*res,numero:1
	});
	starsContainer.add(estrela1);
	estrela1.addEventListener('click',onEstrela);

	var estrela2 = Ti.UI.createView({
		backgroundImage:'images/filtro1/estrela.png',left:6*rw,width:44.5*res,height:42*res,numero:2
	});
	starsContainer.add(estrela2);
	estrela2.addEventListener('click',onEstrela);

	var estrela3 = Ti.UI.createView({
		backgroundImage:'images/filtro1/estrela.png',left:6*rw,width:44.5*res,height:42*res,numero:3
	});
	starsContainer.add(estrela3);
	estrela3.addEventListener('click',onEstrela);

	var estrela4 = Ti.UI.createView({
		backgroundImage:'images/filtro1/estrela.png',left:6*rw,width:44.5*res,height:42*res,numero:4
	});
	starsContainer.add(estrela4);
	estrela4.addEventListener('click',onEstrela);

	var estrela5 = Ti.UI.createView({
		backgroundImage:'images/filtro1/estrela.png',left:6*rw,width:44.5*res,height:42*res,numero:5
	});
	starsContainer.add(estrela5);
	estrela5.addEventListener('click',onEstrela);

	function onEstrela(e){
		switch(e.source.numero){
			case 1:
				filtroEstrelas = '1';
				estrela1.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela2.setBackgroundImage('images/filtro1/estrela.png');
				estrela3.setBackgroundImage('images/filtro1/estrela.png');
				estrela4.setBackgroundImage('images/filtro1/estrela.png');
				estrela5.setBackgroundImage('images/filtro1/estrela.png');
			break;

			case 2:
				filtroEstrelas = '2';
				estrela1.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela2.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela3.setBackgroundImage('images/filtro1/estrela.png');
				estrela4.setBackgroundImage('images/filtro1/estrela.png');
				estrela5.setBackgroundImage('images/filtro1/estrela.png');
			break;

			case 3:
				filtroEstrelas = '3';
				estrela1.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela2.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela3.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela4.setBackgroundImage('images/filtro1/estrela.png');
				estrela5.setBackgroundImage('images/filtro1/estrela.png');
			break;

			case 4:
				filtroEstrelas = '4';
				estrela1.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela2.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela3.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela4.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela5.setBackgroundImage('images/filtro1/estrela.png');
			break;

			case 5:
				filtroEstrelas = '5';
				estrela1.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela2.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela3.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela4.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
				estrela5.setBackgroundImage('images/filtro1/estrelaSelecionada.png');
			break;
		}
	}

	//PREÇOS
	var tituloPrecos = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/precos.png',
		top:20*rh,width:320*res,height:49*res,
	});
	scroll.add(tituloPrecos);

	var precosContainer = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/fieldPreco.png',
		top:30*rh,width:145*res,height:60*res,
	});
	scroll.add(precosContainer);

	var filtrarAte = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/filtrarAte.png',
		left:5*rw,top:30*rh,width:91*res,height:14*res,
	});
	scroll.add(filtrarAte);

	var sliderContainer = Titanium.UI.createView({
		backgroundImage:'/images/filtro1/fieldSlider.png',
		top:9*rh,width:297*res,height:61*res,
	});
	scroll.add(sliderContainer);

	//SLIDER
	var slider = Titanium.UI.createSlider({
	    top:0,width:280*res,
	    min:0,max:1200,value:0,tintColor:'white'
    });
    sliderContainer.add(slider);

    var lbPreco = Ti.UI.createLabel({
		width:130*res,height:55*res,
		bubbleParent:false,color:'white',text:'R$ 0,00',
		font:{fontSize:21*res,},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
	});
	precosContainer.add(lbPreco);

	slider.addEventListener('change', function(e) {
		var numero = Math.floor(e.value);
		filtroPreco = numero;
		var valor = numero.toString();
	    lbPreco.text = 'R$ ' + valor + ',00';
	});


	//BUSCAR & LIMPAR BUTTONS
	var btSearchContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:20*rh,height:100*res
	});
	scroll.add(btSearchContainer);

	var btSearch = Titanium.UI.createView({
		backgroundImage:'images/filtro1/btBuscar.jpg',width:160*res,height:88*res,right:2*rw
	});
	btSearchContainer.add(btSearch);

	btSearch.addEventListener('click',function(e){
		//BUSCAR
		lbNome.blur();
		lbRede.blur();

		var filtroObj = {
			filtroNome:filtroNome.toLowerCase(),
			filtroRede:filtroRede.toLowerCase(),
			filtroTipo:filtroTipo.toLowerCase(),
			filtroPais:filtroPais.toLowerCase(),
			filtroEstado:filtroEstado,
			filtroCidade:filtroCidade.toLowerCase(),
			filtroCatQuarto:filtroCatQuarto,
			filtroTipoQuarto:filtroTipoQuarto,
			filtroEstrelas:filtroEstrelas,
			filtroPreco:filtroPreco + ",00",
		};
		
		if(filtroNome == "" && filtroRede == "" && filtroEstado == "" && filtroCidade == "" && filtroCatQuarto == ""
			&& filtroTipoQuarto == "" && filtroEstrelas == "0" && filtroPreco == 0
		){
			win.close();
		}else{
			activityIndicatorContainer.show();
			Ti.App.Properties.currentSession = 'corporativo';
			var listaFiltroWindow = require('listaFiltro');
			var listaFiltro = listaFiltroWindow(filtroObj);
			listaFiltro.open();
			win.close();
		}
	});

	var btLimpar = Titanium.UI.createView({
		backgroundImage:'images/filtro1/btLimpar.jpg',width:160*res,height:88*res,left:0
	});
	btSearchContainer.add(btLimpar);

	btLimpar.addEventListener('click',function(e){
		//LIMPAR CAMPOS
		lbNome.blur();
		lbRede.blur();
		lbNome.setValue('Nome');
		lbRede.setValue('Rede');
		lbCatQuartos.setText('-');
		lbTipoQuartos.setText('-');
		estrela1.setBackgroundImage('images/filtro1/estrela.png');
		estrela2.setBackgroundImage('images/filtro1/estrela.png');
		estrela3.setBackgroundImage('images/filtro1/estrela.png');
		estrela4.setBackgroundImage('images/filtro1/estrela.png');
		estrela5.setBackgroundImage('images/filtro1/estrela.png');
		slider.setValue(0);

		filtroNome = '';
		filtroRede = '';
		filtroEstado = '';
		filtroCidade = '';
		filtroCatQuarto = '';
		filtroTipoQuarto = '';
		filtroEstrelas = '0';
		filtroPreco = 0;
	});

	win.add(scroll);
	win.add(activityIndicatorContainer);

	return win;
}

module.exports = filtro;