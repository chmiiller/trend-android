function tarifas(pHotel) {

	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#243649',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
	});
	
	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	var hotel = pHotel;

	var singleBalcaoValue = 0;
	var singleAcordoValue = 0;

	var doubleBalcaoValue = 0;
	var doubleAcordoValue = 0;

	var validade = '';
	var taxa = '';
	
		

		if(hotel.Quartos.length >= 1)
		{
			if(hotel.Quartos[0]){
				if(hotel.Quartos[0].Precos.length >= 1){
					if(hotel.Quartos[0].Precos[0].PrecoBalcao){
						var rawPrecoBalcao = hotel.Quartos[0].Precos[0].PrecoBalcao.split(",");
						singleBalcaoValue = rawPrecoBalcao[0];
					}else{
						singleBalcaoValue = 'sob consulta';
					}

					if(hotel.Quartos[0].Precos[0].PrecoAcordo){
						var rawPrecoAcordo = hotel.Quartos[0].Precos[0].PrecoAcordo.split(",");
						singleAcordoValue = rawPrecoAcordo[0];
					}else{
						singleAcordoValue = 'sob consulta';
					}

					if(hotel.Quartos[0].Precos[0].PrecoBalcaoDBL){
						var rawPrecoBalcaoDBL = hotel.Quartos[0].Precos[0].PrecoBalcaoDBL.split(",");
						doubleBalcaoValue = rawPrecoBalcaoDBL[0];
					}else{
						doubleBalcaoValue = 'sob consulta';
					}
					
					if(hotel.Quartos[0].Precos[0].PrecoAcordoDBL){
						var rawPrecoAcordoDBL = hotel.Quartos[0].Precos[0].PrecoAcordoDBL.split(",");
						doubleAcordoValue = rawPrecoAcordoDBL[0];
					}else{
						doubleAcordoValue = 'sob consulta';
					}
					
					if(hotel.Quartos[0].Precos[0].Validade != ''){
						var rawValidade = hotel.Quartos[0].Precos[0].Validade.split("/");
						var dia = rawValidade[1];
						var mes = rawValidade[0];
						var ano = rawValidade[2];

						validade = dia + '/' + mes + '/' + ano;
					}

					if(hotel.Quartos[0].Precos[0].Taxa != ''){
						taxa = hotel.Quartos[0].Precos[0].Taxa;
					}
				}
			}
			
		}
	

	var buttonsViewContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:50*rh,left:0,width:Ti.UI.FILL,height:48.5*res
	});
	win.add(buttonsViewContainer);

	var btAcordo = Titanium.UI.createButton({
		backgroundImage:'images/precos/btAcordoOver.png',top:0,left:0,width:160.5*res,height:48.5*res,data:'acordo',
		backgroundSelectedImage:'images/precos/btAcordoOver.png'
	});
	buttonsViewContainer.add(btAcordo);

	var btBalcao = Titanium.UI.createButton({
		backgroundImage:'images/precos/btBalcao.png',top:0,left:161*rw,width:159.5*res,height:48.5*res,data:'balcao',
		backgroundSelectedImage:'images/precos/btBalcaoOver.png'
	});
	buttonsViewContainer.add(btBalcao);

	btAcordo.addEventListener('click',handleTopButtons);
	btBalcao.addEventListener('click',handleTopButtons);

	function handleTopButtons(e){
		switch(e.source.data){
			case 'acordo':
				btAcordo.setBackgroundImage('images/precos/btAcordoOver.png');
				btBalcao.setBackgroundImage('images/precos/btBalcao.png');
				populateAcordo();
			break;

			case 'balcao':
				btAcordo.setBackgroundImage('images/precos/btAcordo.png');
				btBalcao.setBackgroundImage('images/precos/btBalcaoOver.png');
				populateBalcao();
			break;
		}
	}

	function populateAcordo(){
		formatLabels('acordo');
	}

	function populateBalcao(){
		formatLabels('balcao');
	}


	var divisoria = Titanium.UI.createView({
		backgroundImage:'images/precos/divisoria.png',top:250*rh,left:0,width:Ti.UI.FILL,height:1*res
	});
	win.add(divisoria);
	
	
	//LABELS SGL - SINGLE	
	var singleLabel = Titanium.UI.createLabel({
		text:'SGL',color:'white',font:{fontSize:15*res},
		top:170*rh,left:30*rw
	});
	win.add(singleLabel);

	var rsLabel = Titanium.UI.createLabel({
		text:'R$',color:'white',font:{fontSize:18*res},
		top:140*rh,left:90*rw
	});
	win.add(rsLabel);

	var singleValueLabel = Titanium.UI.createLabel({
		text:singleAcordoValue + ',',color:'white',font:{fontSize:75*res},
		top:140*rh,left:115*rw
	});
	win.add(singleValueLabel);

	var centsValueLabel = Titanium.UI.createLabel({
		text:'00',color:'white',font:{fontSize:32*res},
		top:140*rh,right:25*rw
	});
	win.add(centsValueLabel);
	

	//LABELS DBL - DOUBLE
	var doubleLabel = Titanium.UI.createLabel({
		text:'DBL',color:'white',font:{fontSize:15*res},
		top:310*rh,left:30*rw
	});
	win.add(doubleLabel);

	var rsLabel2 = Titanium.UI.createLabel({
		text:'R$',color:'white',font:{fontSize:18*res},
		top:280*rh,left:90*rw
	});
	win.add(rsLabel2);

	var doubleValueLabel = Titanium.UI.createLabel({
		text:doubleAcordoValue + ',',color:'white',font:{fontSize:75*res},
		top:280*rh,left:115*rw
	});
	win.add(doubleValueLabel);

	var centsValueLabel2 = Titanium.UI.createLabel({
		text:'00',color:'white',font:{fontSize:32*res},
		top:280*rh,right:25*rw
	});
	win.add(centsValueLabel2);


	formatLabels('acordo');
	function formatLabels(tipo){
		if(tipo == 'acordo'){
			singleValueLabel.setText(singleAcordoValue + ',');
			doubleValueLabel.setText(doubleAcordoValue + ',');
			if(singleAcordoValue >= 1000 && singleAcordoValue < 10000){
				singleValueLabel.setFont({fontSize:65*res});
				centsValueLabel.setRight(10*rw);
			}else if(singleAcordoValue >= 10000){
				singleValueLabel.setText(singleAcordoValue);
				singleValueLabel.setFont({fontSize:63*res});
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}else if(singleAcordoValue == 'sob consulta' || singleAcordoValue == 'Sob Consulta' || singleAcordoValue == 'Sob consulta'){
				singleValueLabel.setText('Sob Consulta');
				singleValueLabel.setFont({fontSize:20*res});
				singleValueLabel.setTop(160*rh);
				rsLabel.setVisible(false);
				rsLabel2.setVisible(false);
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}

			if(doubleAcordoValue >= 1000 && doubleAcordoValue < 10000){
				doubleValueLabel.setFont({fontSize:65*res});
				centsValueLabel.setRight(10*rw);
			}else if(doubleAcordoValue >= 10000){
				doubleValueLabel.setText(doubleAcordoValue);
				doubleValueLabel.setFont({fontSize:63*res});
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}else if(doubleAcordoValue == 'sob consulta' || doubleAcordoValue == 'Sob Consulta' || doubleAcordoValue == 'Sob consulta'){
				doubleValueLabel.setText('Sob Consulta');
				doubleValueLabel.setFont({fontSize:20*res});
				doubleValueLabel.setTop(305*rh);
				rsLabel.setVisible(false);
				rsLabel2.setVisible(false);
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}

		}else{
			singleValueLabel.setText(singleBalcaoValue + ',');
			doubleValueLabel.setText(doubleBalcaoValue + ',');
			if(singleBalcaoValue >= 1000 && singleBalcaoValue < 10000){
				singleValueLabel.setFont({fontSize:65*res});
				centsValueLabel.setRight(10*rw);
			}else if(singleBalcaoValue >= 10000){
				singleValueLabel.setText(singleBalcaoValue);
				singleValueLabel.setFont({fontSize:63*res});
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}else if(singleBalcaoValue == 'sob consulta' || singleBalcaoValue == 'Sob Consulta' || singleBalcaoValue == 'Sob consulta'){
				singleValueLabel.setText('Sob Consulta');
				singleValueLabel.setFont({fontSize:20*res});
				singleValueLabel.setTop(160*rh);
				rsLabel.setVisible(false);
				rsLabel2.setVisible(false);
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}

			if(doubleBalcaoValue >= 1000 && doubleBalcaoValue < 10000){
				doubleValueLabel.setFont({fontSize:65*res});
				centsValueLabel.setRight(10*rw);
			}else if(doubleBalcaoValue >= 10000){
				doubleValueLabel.setText(doubleBalcaoValue);
				doubleValueLabel.setFont({fontSize:63*res});
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}else if(doubleBalcaoValue == 'sob consulta' || doubleBalcaoValue == 'Sob Consulta' || doubleBalcaoValue == 'Sob consulta'){
				doubleValueLabel.setText('Sob Consulta');
				doubleValueLabel.setFont({fontSize:20*res});
				doubleValueLabel.setTop(305*rh);
				rsLabel.setVisible(false);
				rsLabel2.setVisible(false);
				centsValueLabel.setVisible(false);
				centsValueLabel2.setVisible(false);
			}
		}
	}

	var divisoria2 = Titanium.UI.createView({
		backgroundImage:'images/precos/divisoria.png',top:395*rh,left:0,width:Ti.UI.FILL,height:1*rh
	});
	win.add(divisoria2);

	var iconNotas = Titanium.UI.createView({
		backgroundImage:'images/precos/iconNotas.png',top:401*rh,left:15*rw,width:34*rw,height:18*rh
	});
	win.add(iconNotas);

	var validadeLabel = Titanium.UI.createLabel({
		text:'Válido até: ' + validade,color:'white',font:{fontSize:13*res},
		top:404*rh,left:65*rw
	});
	win.add(validadeLabel);

	var taxaLabel = Titanium.UI.createLabel({
		text:'- ' + taxa + ' de taxas',color:'white',font:{fontSize:13*res},
		top:404*rh,left:205*rw
	});
	win.add(taxaLabel);
	
	if(taxa == ''){
		taxaLabel.setVisible(false);
		iconNotas.setLeft(70*rw);
		validadeLabel.setLeft(110*rw);
	}	

	var btLegendas = Titanium.UI.createButton({
		backgroundImage:'images/precos/btLegendas.png',bottom:0,left:0,width:316.5*res,height:53*res
	});
	win.add(btLegendas);

	btLegendas.addEventListener('click',function(e){
		var legendaWindow = require('legenda');
		var legenda = legendaWindow();
		legenda.open();
	});

	return win;
}

module.exports = tarifas;