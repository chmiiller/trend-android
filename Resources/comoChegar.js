function comoChegar(pHotel) {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	var gd;

	var hotel = pHotel;

	var brieFont = 'Brie Light';
	var yanoneBold = 'Yanone Kaffeesatz';

	var viewStyle = "grade";

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'white',top:0,left:0,width:320*rw,height:93*rh,
	});
	win.add(header);

	var headerTitle = Titanium.UI.createView({
		backgroundImage:'images/mapa/header.png',top:30*rh,left:0,width:320*rw,height:46.5*rh,opacity:0.9
	});

	//MAIN SCROLL
	var mainScroll = Titanium.UI.createScrollView({
		top:63*rh,left:0,width:320*rw,disableBounce:true,
		backgroundColor:'#f8f8f8'
		
	});

	win.add(mainScroll);
	win.add(headerTitle);

	var containerView = Titanium.UI.createView({
		backgroundImage:'images/mapa/bgInfo2.png',
		top:300*rh,left:0,width:320*rw,height:503.5*rh,
		opacity:0.9
	});

	//DISTANCE LABEL
	var currentDistanceNumber = 0;
	var distanceNumber = 0;
	var distanceMultiplier = 1;
	var distanceLabelInterval;
	
	var distanceLabel = Titanium.UI.createLabel({
		text:'0',font:{fontSize:115*rw,fontFamily:brieFont},color:'white',
		top:10*rh,right:40*rw,
		verticalAlign:Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
	});

	//MAP
	var Map;
	var latitude = hotel.Latitude;
    var longitude = hotel.Longitude;
    var userLatitude;
	var userLongitude;
    var hotelPin;
	var mapView;

	setTimeout(function(){
		Map = require('ti.map');

	    hotelPin = Map.createAnnotation({
		    latitude:latitude,
		    longitude:longitude,
		    title:hotel.Nome,
		    subtitle:hotel.Endereco + ', ' + hotel.Numero,
		    image:'images/detalhes/pin.png'
		});

		mapView = Map.createView({
			width:Ti.UI.FILL,
			top:0,
			height:410*res,
			mapType:Map.NORMAL_TYPE,
			userLocation:true,
			annotations:[hotelPin],
			animate:false,
		});
		mainScroll.add(mapView);

	    mapView.setRegion({
			latitude: latitude,
			longitude: longitude, 
			latitudeDelta:0.01, 
			longitudeDelta:0.01,
		});
		containerView.add(distanceLabel);
		mainScroll.add(containerView);
		getUserLocation();
	},400);

	function getUserLocation(){
		try {
		  	Titanium.Geolocation.getCurrentPosition(function(e){
		        if (!e.success){
		             alert('Não foi possível pegar sua localização');
		             return;
		        }else{
		        	
		        	userLatitude = e.coords.latitude;
		        	userLongitude = e.coords.longitude;
		        	origem = {latitude: userLatitude, longitude: userLongitude};
			        destino = {latitude: latitude, longitude:longitude};
			        
			        distanceNumber = calculateDistance(userLatitude,userLongitude,latitude,longitude, 'K'); //in Km
					
					var font;
					if(distanceNumber >= 100 && distanceNumber < 1000){
						font = {fontSize:77*rw,fontFamily:brieFont};
						distanceLabel.setFont(font);
						distanceLabel.setTop(40*rh);
					}else if(distanceNumber >= 1000 && distanceNumber < 10000){
						font = {fontSize:60*rw,fontFamily:brieFont};
						distanceLabel.setFont(font);
						distanceLabel.setTop(60*rh);
					}

					if(distanceNumber < 10){
			        	distanceNumber = '0' + distanceNumber;
			        }
			        distanceNumber = Math.round(distanceNumber);
			        distanceLabel.setText(distanceNumber);

			        if(distanceNumber < 400){
			        	tracarRota();
			        }
					
		        }
			});
		} catch (err) {
			alert('Não foi possível pegar sua localização');
		}
	}
	
	function tracarRota(){
		gd = require('de.codewire.google.directions');
	    gd.getRoute({
	        origin : origem,
	        destination : destino,
	        color : '#57585b',
	        name : 'single',
	        width: 3*res,
	        callback : function(response) {
	            if(response.status == 'OK') {
	                var rota = Map.createRoute(response.route);
	                mapView.addRoute(rota);
	            } else {
	                alert("Erro ao calcular rota");
	            }
	        }
	    });
	}

	var kmLabel = Titanium.UI.createLabel({
		text:'km',font:{fontSize:28*rw,fontFamily:brieFont},color:'white',
		top:80*rh,right:7*rw,
		textAlign:Ti.UI.TEXT_ALIGNMENT_RIGHT
	});
	containerView.add(kmLabel);

	//NAME LABEL
	var nameLabel = Titanium.UI.createLabel({
		text:hotel.Nome,
		font:{fontSize:25*rw,fontFamily:yanoneBold},color:'white',
		top:17*rh,left:7*rw,width:140*rw,height:30*rh,
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
	});
	containerView.add(nameLabel);

	//STARS
	var starsContainer = Titanium.UI.createView({
		top:50*rh,left:8*rw,width:130*rw,height:30*rh
	});
	containerView.add(starsContainer);
	populateStarsNumber(hotel.Estrelas);

	//BUTTONS
	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/mapa/sobre.png',top:160*rh,left:10*rw,width:264*rw,height:50.5*rh
	});
	containerView.add(btSobre);

	btSobre.addEventListener('click',function(e){
		var detalhesWindow = require('detalhes');
		var detalhes = detalhesWindow(hotel);
		detalhes.open();
	});

	var btFotos = Titanium.UI.createButton({
		backgroundImage:'images/mapa/fotos.png',top:245*rh,left:10*rw,width:268*rw,height:51.5*rh
	});
	containerView.add(btFotos);

	btFotos.addEventListener('click',function(e){
		var fotosWindow = require('fotos');
		var fotos = fotosWindow(hotel);
		fotos.open();
	});

	var btTarifas = Titanium.UI.createButton({
		backgroundImage:'images/mapa/tarifas.png',top:335*rh,left:10*rw,width:266.5*rw,height:56*rh
	});
	containerView.add(btTarifas);

	btTarifas.addEventListener('click',function(e){
		var tarifasWindow = require('tarifas');
		var tarifas = tarifasWindow(hotel);
		tarifas.open();
	});

	var btRotas = Titanium.UI.createButton({
		backgroundImage:'images/mapa/rotas.png',top:425*rh,left:18*rw,width:261.5*rw,height:55*rh
	});
	containerView.add(btRotas);

	btRotas.addEventListener('click',function(e){
		var dialog = Ti.UI.createOptionDialog({
			title:'Traçar rota usando:',
			cancel: 2,
			options: ['Google Maps', 'Waze', 'Cancelar'],
		});
		dialog.show();
		dialog.addEventListener('click',function(e){
			switch(e.index){
				case 0:
                    Ti.Platform.openURL('http://maps.google.com/maps?daddr=' + latitude + ',' + longitude);
                break;
    
                case 1:
                	if(!Ti.Platform.openURL('waze://?ll=' + latitude + ',' + longitude + "&navigate=yes")){
                        Ti.Platform.openURL("market://details?id=com.waze");
                    }
                break;
			}
		});
	});

	function populateStarsNumber(starsCount){
		var posX = 20;
		for(var i = 0; i < starsCount; i++){
			var star = Titanium.UI.createView({
				backgroundImage:'images/mapa/star.png',left:posX*rw,width:13*rw,height:12.5*rh
			});
			posX += 17;
			starsContainer.add(star);
		}
	}

	function calculateDistance(lat1, lon1, lat2, lon2, unit) {
	    var radlat1 = Math.PI * lat1/180;
	    var radlat2 = Math.PI * lat2/180;
	    var radlon1 = Math.PI * lon1/180;
	    var radlon2 = Math.PI * lon2/180;
	    var theta = lon1-lon2;
	    var radtheta = Math.PI * theta/180;
	    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	    dist = Math.acos(dist);
	    dist = dist * 180/Math.PI;
	    dist = dist * 60 * 1.1515;
	    if (unit=="K") { dist = dist * 1.609344; };
	    if (unit=="N") { dist = dist * 0.8684; };
	    return dist;
	}

	return win;
}

module.exports = comoChegar;