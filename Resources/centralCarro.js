function centralCarro() {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		navBarHidden : true,
		fullscreen : true,
		backgroundColor : '#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	var brieFont = 'Brie Light';
	var yanoneBold = 'Yanone Kaffeesatz';

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor : 'white',
		top : 0,
		left : 0,
		width : 320*res,
		height : 65*res
	});

	win.add(header);

	var logoTrend = Titanium.UI.createView({
		backgroundImage:'images/trendLogo.png',top:28*rh,left:40*rw,width:79*res,height:32*res
	});
	header.add(logoTrend);
	
	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSobre.png',top:26*rh,left:160*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSobre);
	btSobre.addEventListener('click', function(e) {
		var sobreWindow = require('sobre');
		var sobre = sobreWindow();
		sobre.open();
	});

	var btContato = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btContato.png',top:26*rh,left:206*rw,width:30.5*res,height:30.5*res
	});
	header.add(btContato);

	btContato.addEventListener('click',function(e){
		var contatoWindow = require('contato');
		var contato = contatoWindow();
		contato.open();
	});

	var btSearch = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSearch.png',top:26*rh,left:250*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSearch);
	btSearch.addEventListener('click', function(e) {
		var filtroWindow = require('filtro');
		var filtro = filtroWindow();
		filtro.open();
	});

	//TITLE
	var title = Titanium.UI.createView({
		backgroundImage : 'images/central/tituloCentraldoCarro.png',
		top : 66*rh,
		left : 0,
		width : 320*res,
		height : 50.5*res
	});
	win.add(title);

	var base = Ti.UI.createView({
		top : 117*rh,
		width : 320*res,
		height : Ti.UI.FILL,
	});

	win.add(base);

	var tabelaEmpresas = Ti.UI.createTableView({
		width : 320*res,
		height : Ti.UI.FILL,
	});

	var tabelaEmpresasData = [];

	var avisRow = Ti.UI.createTableViewRow({
		height : 108*res,
		value : 'Avis',
		width : 432*res,
		backgroundImage : 'images/central/rowAvis.jpg'
	});

	tabelaEmpresasData.push(avisRow);

	var hertzRow = Ti.UI.createTableViewRow({
		height : 108*res,
		width : 320*res,
		value : 'Hertz',
		backgroundImage : 'images/central/rowHertz.jpg'
	});
	tabelaEmpresasData.push(hertzRow);

	var movidaRow = Ti.UI.createTableViewRow({
		height : 108*res,
		value : 'Movida Rent a Car',
		width : 320*res,
		backgroundImage : 'images/central/rowMovida.jpg'
	});
	tabelaEmpresasData.push(movidaRow);

	var UnidasRow = Ti.UI.createTableViewRow({
		height : 108*res,
		width : 320*res,
		value : 'Unidas',
		backgroundImage : 'images/central/rowUnidas.jpg'
	});
	tabelaEmpresasData.push(UnidasRow);

	tabelaEmpresas.setData(tabelaEmpresasData);

	base.add(tabelaEmpresas);

	tabelaEmpresas.addEventListener('click', function(e) {
		var mainArray = [];
		var mainArrayLength = 0;

		var file = null;
		var blob = null;
		var readText = null;
		var data = null;

		file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "trend.txt");
		if(file.exists()){
			blob = file.read();
			if(blob){
				readText = blob.text;
				data = JSON.parse(readText);
				mainArray = data.Locadora;
				mainArrayLength = mainArray.length;
				
				file = null;
				blob = null;
				readText = null;
			}
		}

		var locadora = null;
		for(var i = 0; i < mainArrayLength; i++){
			var _item = mainArray[i];
			if(_item.Nome == e.row.value){
				locadora = _item;
			}
		}

		if(locadora != null){
			var centralCarroInfoWindow = require('centralCarroInfos');
			var centralCarroInfo = centralCarroInfoWindow(e.row.value);
			centralCarroInfo.open();
		}else{
			alert('Locadora não cadastrada');
		}
		
	});

	return win;
}

module.exports = centralCarro;
