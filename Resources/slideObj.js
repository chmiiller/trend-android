function slideObj(callback,data) {

	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;

	fakedata = {
		image:'http://www.hotelschool.co.za/files/2011/12/Hotel-Resort.jpg',
		title:'Hotel Resort',
		starsCount:3
	};

	var mainImage = fakedata.image;
	var id = data.Id;
	var title = data.Nome;
	var cidade = data.Cidade.Nome + ' / ' + data.Cidade.Estado.Nome;
	var starsCount = data.Estrelas;
	var novaStrFoto = data.Foto.replace(/ /gi, "%20");
	var imageUrl = Titanium.App.Properties.mainUrl + novaStrFoto;

	var win = Titanium.UI.createView({
		backgroundImage:'images/lista/hotelContainer.png',
		top:0,width:241.5*rw,height:287*rh
	});

/*	if(Ti.Platform.displayCaps.platformHeight > 1281){
		win.setTop(20*rh);
		win.setWidth(281.5*rw);
		win.setHeight(337*rh);
	}*/

	var imageView = Titanium.UI.createImageView({
		image:imageUrl,top:3*rh,width:238*rw,height:159*rh,
		defaultImage:'images/defaultImageSlide.jpg'
	});
	win.add(imageView);

	var reflexo = Titanium.UI.createView({
		backgroundImage:'images/lista/reflexo.png',top:3*rh,width:278*rw,height:223*rh
	});

	win.add(reflexo);

	var lbTitle = Titanium.UI.createLabel({
		text:title,top:180*rh,left:0,width:238*rw,height:20*rh,
		color:'#4e4e4e',font:{fontSize:17*rw},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
	});
	win.add(lbTitle);

	//CITY
	var lbCidade = Titanium.UI.createLabel({
		text:cidade,
		color:'#4e4e4e',
		backgroundColor:'transparent',
		top:195*rh,left:0,
		width:Ti.UI.FILL,height:20*rh,
		font:{fontSize:12*rw},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
	});
	win.add(lbCidade);

	var starsContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:230*rh,width:160*rw,height:25*rh,
	});
	var starAlignCenter = Titanium.UI.createView({
		backgroundColor:'transparent',layout:'horizontal',width:Ti.UI.SIZE,height:Ti.UI.SIZE
	});
	win.add(starsContainer);
	starsContainer.add(starAlignCenter);
	populateStarsNumber(starsCount);


	win.addEventListener('click',function(e){
		callback(id);
	});

	function populateStarsNumber(starsCount){
		for(var i = 0; i < starsCount; i++){
			var star = Titanium.UI.createView({
				backgroundImage:'images/lista/estrelaSlide.png',left:3*rw,width:17.5*rw,height:16.5*rh
			});
			starAlignCenter.add(star);
		}
	}

	return win;
}

module.exports = slideObj;