function feiras() {

	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });
    
	var brieFont = 'Brie Light';
	var yanoneBold = 'Yanone Kaffeesatz';

	var imageUrl = Titanium.App.Properties.mainUrl;

	var hotelArray = [];
	var hotelArrayLength = 0;

	var file = null;
	var blob = null;
	var readText = null;
	var data = null;

	file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "trend.txt");
	if(file.exists()){
		blob = file.read();
		if(blob){
			readText = blob.text;
			data = JSON.parse(readText);
			hotelArray = data.Feira;
			hotelArrayLength = hotelArray.length;
			Titanium.App.Properties.currentSession = 'feira';

			file = null;
			blob = null;
			readText = null;
		}
	}

	var selectedArray = [];
	
	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'white',top:0,left:0,width:320*res,height:65*res
	});

	win.add(header);

	var logoTrend = Titanium.UI.createView({
		backgroundImage:'images/trendLogo.png',top:28*rh,left:40*rw,width:79*res,height:32*res
	});
	header.add(logoTrend);

	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSobre.png',top:26*rh,left:160*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSobre);
	btSobre.addEventListener('click',function(e){
		var sobreWindow = require('sobre');
		var sobre = sobreWindow();
		sobre.open();
	});

	var btContato = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btContato.png',top:26*rh,left:206*rw,width:30.5*res,height:30.5*res
	});
	header.add(btContato);

	btContato.addEventListener('click',function(e){
		var contatoWindow = require('contato');
		var contato = contatoWindow();
		contato.open();
	});
	
	var btSearch = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSearch.png',top:26*rh,left:250*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSearch);
	btSearch.addEventListener('click',function(e){
		var filtroWindow = require('filtro');
		var filtro = filtroWindow();
		filtro.open();
	});


	//TITLE
	var title = Titanium.UI.createView({
		backgroundImage:'images/feiras/tituloFeiras.png',top:66*rh,left:0,width:320*res,height:50.5*res
	});
	win.add(title);

	//MESES NAVIGATION
	var navigation = Ti.UI.createView({
		backgroundColor:'#0177bf',top:116*rh,left:0,width:Ti.UI.FILL,height:48*res
	});
	win.add(navigation);

	var btPrev = Titanium.UI.createButton({
		backgroundImage:'images/feiras/btPrev.jpg',left:0,width:48*res,height:48*res
	});
	btPrev.addEventListener('click',function(e){
		if(scrollableText.currentPage > 0){
			scrollableText.scrollToView(scrollableText.currentPage-1);
		}
	});

	var btNext = Titanium.UI.createButton({
		backgroundImage:'images/feiras/btNext.jpg',right:0,width:48*res,height:48*res
	});
	btNext.addEventListener('click',function(e){
		var _arrayTotalSize = scrollableText.getViews().length - 1;
		if(scrollableText.currentPage < _arrayTotalSize){
			scrollableText.scrollToView(scrollableText.currentPage+1);
		}
	});

	//MESES E SCROLLABLE
	var mesesArray = ['Janeiro','Fevereiro','Marco','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
	var currentDate = new Date();

	var scrollableText = Titanium.UI.createScrollableView({
		top:0,left:0,width:Ti.UI.FILL,height:Ti.UI.FILL
	});
	scrollableText.addEventListener('scrollend',function(e){
		var formattedMes = scrollableText.currentPage + 1;
		if(formattedMes < 10){
			formattedMes = '0' + formattedMes;
		}
		filterFeiras(formattedMes,false);
	});

	var viewsArray = [];
	var mesesArraySize = mesesArray.length;
	for(var i = 0; i < mesesArraySize; i++){
		var _item = mesesArray[i];
		var _view = Ti.UI.createView({
			top:0,left:0,width:Ti.UI.FILL,height:Ti.UI.FILL
		});
		var lbMes = Ti.UI.createLabel({
			text:_item,font:{fontSize:25*res,fontFamily:brieFont},color:'white',
			verticalAlign:Titanium.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
		});
		_view.add(lbMes);
		viewsArray.push(_view);
	}
	scrollableText.setViews(viewsArray);
	scrollableText.scrollToView(currentDate.getMonth());
	
	navigation.add(scrollableText);
	
	navigation.add(btPrev);
	navigation.add(btNext);

	//MAIN PICTURE
	var mainPicture = Ti.UI.createView({
		backgroundColor:'white',top:163*rh,left:0,width:Ti.UI.FILL,height:122*res
	});
	win.add(mainPicture);

	//SOMBRA
	var sombra = Ti.UI.createView({
		backgroundImage:'images/feiras/sombraFrame.png',top:274*rh,left:0,width:Ti.UI.FILL,height:11.5*res
	});
	win.add(sombra);

	//TABLE
	var table = Titanium.UI.createTableView({
		top:165*rh,left:0,width:Ti.UI.FILL,height:Ti.UI.FILL,
		backgroundColor:'#f1f2f2'
	});
	win.add(table);

	//ROWS
	var rowData = [];

	var formattedMes = currentDate.getMonth() + 1;
	if(formattedMes < 10){
		formattedMes = '0' + formattedMes;
	}

	filterFeiras(formattedMes,true);

	function filterFeiras(mes,primeiraVez){
		selectedArray = [];
		for(var i = 0; i < hotelArrayLength; i++){
			var _item = hotelArray[i];
			var _itemMesRaw = _item.DataInicial.split('/');
			var _itemMes = (_itemMesRaw[0]);
			if(_itemMes == mes){
				selectedArray.push(_item);
			}
		}
		if(selectedArray.length > 0){
			updateTable();
		}else{
			rowData = [];
			
			var _row = Titanium.UI.createTableViewRow({
				top:0,left:0,width:Ti.UI.FILL,height:89*res
			});

			var lbSemResultados = Ti.UI.createLabel({
				color: '#717171',
			  	font: {fontFamily:'Helvetica Neue', fontSize:19*res},
				text:'Nenhum Resultado Encontrado'
			});

			_row.add(lbSemResultados);
			rowData.push(_row);

			table.setData(rowData);
		}
		
	}

	function updateTable(){
		rowData = [];
		table.setData(rowData);
		for(var i = 0; i < selectedArray.length; i++){
			var _item = selectedArray[i];
			var _row = Titanium.UI.createTableViewRow({
				backgroundImage:'images/feiras/row.jpg',top:0,left:0,width:Ti.UI.FILL,height:89*res
			});

			var logoFeira = Titanium.UI.createImageView({
				image:imageUrl + _item.Logo,top:8*rh,left:10*rw,width:90*res,height:70*res,
			});
			_row.add(logoFeira);

			var lbNomeFeira = Ti.UI.createLabel({
				text:_item.Nome,font:{fontSize:15*res,fontWeight:'bold'},
				color:'#6d6e71',top:8*rh,left:125*rw,width:195*res,height:20*res,
			});
			_row.add(lbNomeFeira);

			var lbLocal = Ti.UI.createLabel({
				text:_item.Local,font:{fontSize:14*res,fontWeight:'bold'},
				color:'#6d6e71',top:28*rh,left:125*rw,width:195*res,height:20*res,
			});
			_row.add(lbLocal);

			var lbDatas = Ti.UI.createLabel({
				text:_item.DataInicial + ' à ' + _item.DataFinal,font:{fontSize:13*res},
				color:'#6d6e71',top:45*rh,left:125*rw,width:195*res,height:20*res,
			});
			_row.add(lbDatas);

			var lbCategoria = Ti.UI.createLabel({
				text:_item.Descricao,font:{fontSize:13*res},
				color:'#6d6e71',top:59*rh,left:125*rw,width:195*res,height:20*res,
			});
			_row.add(lbCategoria);

			rowData.push(_row);
		}
		table.setData(rowData);
	}

	return win;
}

module.exports = feiras;