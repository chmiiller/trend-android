function sobre() {

	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		fullscreen: true,
		navBarHidden: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'white',top:0,left:0,width:320*rw,height:65*rh
	});

	win.add(header);

	var logoTrend = Titanium.UI.createView({
		backgroundImage:'images/trendLogo.png',top:23*rh,left:40*rw,width:79*res,height:32*res
	});
	header.add(logoTrend);

	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSobre.png',top:21*rh,left:160*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSobre);

	var btContato = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btContato.png',top:21*rh,left:206*rw,width:30.5*res,height:30.5*res
	});
	header.add(btContato);
	
	var btSearch = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSearch.png',top:21*rh,left:250*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSearch);


	//BOX
	var boxView = Titanium.UI.createView({
		backgroundImage:'images/sobre/box.png',top:66*rh,width:290*rw,height:195*rh
	});
	win.add(boxView);

	var texto1 = '<font color="#5a5a5a" face="helvetica" size="1"><b>PALAVRA DO PRESIDENTE</b> <br><br>Amigos, <br>Inovação está no DNA da TREND e isso não se resume à busca de soluções internas. Inovamos em tudo, inclusive e principalmente na comunicação com nossos parceiros. Foi com esse conceito em mente que nossa equipe trabalhou duro para entregar a vocês o novo e moderno portal institucional da TREND, um dos diversos meios de interação da operadora com o mundo. <br><br>Com navegação simplificada, linguagem acessível, interface clean e amigável, o portal utiliza tecnologia de ponta e traz tudo o que você precisa saber sobre a TREND, além de oferecer ferramentas úteis para o dia a dia. Os dados institucionais, endereços da matriz e das filiais em operação nas principais cidades do país, contatos dos escritórios de Buenos Aires e Miami, e todas as informações sobre os produtos nacionais e internacionais comercializados foram organizados de forma mais clara para facilitar o acesso. <br><br>A grande inovação, entretanto, é a inclusão constante de pacotes de lazer que são atualizados frequentemente e têm link direto para o portal de vendas. Considero esta a maior e mais importante mudança, pois sei que o tempo do agente de viagens é escasso. A homepage exibe ainda ofertas de hotelaria nacional e internacional, locação de carros e links diretos para o Diretório TREND 2014, Portal de Eventos, TREND Mega Partner, Revista Segue Viagem, para as redes sociais e para os sistemas de autoatendimento (agência e hotel). <br><br>A rotina de quem trabalha com turismo é bastante dinâmica e o portal vem em boa hora, como um presente da TREND para facilitar a vida de nossos clientes e parceiros. Navegue bastante e, se precisar, entre em contato pela aba “fale conosco” que agora permite o direcionamento das mensagens para o departamento adequado. É a TREND economizando seu tempo para que você aproveite cada minuto para vender mais! <br><br>Um forte abraço, <br>Luis Paulo LUPPA</font><br><img src="https://photos-2.dropbox.com/t/0/AAAaY30byNPmds5eTQcrcd2J5G3wdJ4ggRciPRCCqRerZQ/12/10457367/jpeg/1024x768/3/1403769600/0/2/presidente.jpg/F-kJ71cIzi6jP2hsYAk9dcMKt8BpPi6w0PvL0rvDjyw">';
	var texto2 = '<font color="#5a5a5a" face="helvetica" size="1"><b>NOSSA HISTÓRIA</b> <br><br>Foi numa pequena sala no centro de São Paulo, no ano de 1992, que surgiu a TREND operadora, que inicialmente trabalhava apenas com hotelaria nacional. Aos poucos a estrutura foi crescendo, incorporando mais hotéis e produtos, como locação de carros e serviços de receptivo, hotelaria internacional. No ano de 2008, ingressou na história da TREND, Luis Paulo Luppa, sócio e presidente da empresa, que deflagrou um processo irreversível de crescimento e profissionalização.<br><br>Hoje a Trend Operadora é considerada a maior empresa em soluções para viagens e turismo do Brasil e oferece um dos sistemas mais modernos de reservas do mundo, hotéis nacionais e internacionais, as melhores opções de aéreo, aluguel de carros em diversos países, serviços de receptivos, além de vários outros produtos que agregam valor às opções de hospedagem.<br><br>Com 21 anos no mercado, possui matriz instalada no centro da cidade de São Paulo, filiais por todo o Brasil e também em Miami, e mais de 800 profissionais altamente qualificados.<br><br>Uma estrutura completa para oferecer a todos os nossos clientes a melhor experiência em vender viagens.</font>';
	var texto3 = '<font color="#5a5a5a" face="helvetica" size="1"><b>MISSÃO, VISÃO E VALORES</b> <br><br><b>MISSÃO</b><br>Garantir facilidades para nossos clientes, fornecedores e colaboradores por meio de soluções integradas e inovadoras em viagens.<br><br><br><b>VISÃO</b><br>Dominar mercados, tornando a TREND OPERADORA referência do Brasil para o mundo e do mundo para o Brasil.<br><br><br><b>VALORES</b><br>1. Foco do CLIENTE<br>Antecipar-se e adaptar-se às necessidades do Cliente, oferecendo melhores oportunidades, atuando sempre com comprometimento e credibilidade, visando a fidelidade.<br><br>2. Eficiência e Inovação dos PROCESSOS<br>Disponibilizar soluções integradas de alta qualidade e garantir a inteligência dos processos, tornando-as inovadoras, eficientes, confiáveis e rentáveis.<br><br>3. Capacitação e Valorização das PESSOAS<br>As pessoas são os pilares do nosso desenvolvimento, portanto sua capacitação, valorização e iniciativas são fundamentais para o nosso sucesso. O respeito, o trabalho em equipe, o comprometimento, a humildade e a simplicidade são valores vitais. Nossa responsabilidade social está em preservar a qualidade de vida de nossos colaboradores e perenizar a empresa.<br><br>4. Obtenção de RESULTADOS<br>O nosso resultado é consequência da eficácia das estratégias conjuntas com os nossos Clientes e Fornecedores, sustentado pela prática de nossos valores.</font>';
	
	var txtSobre = Titanium.UI.createWebView({
		top:262*rh,left:0,width:320*res,height:190*res,
		backgroundColor:'transparent',
		html:texto1,
		softKeyboardOnFocus: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS,
		enableZoomControls:false
	});
	win.add(txtSobre);

	var fade = Titanium.UI.createView({
		backgroundImage:'images/sobre/fade.png',top:470*rh,left:0,width:320*rw,height:22*rh
	});
	win.add(fade);

	var divisoria = Titanium.UI.createView({
		backgroundImage:'images/sobre/divisoria.png',top:495*rh,left:10*rw,width:291*rw,height:1*rh
	});
	win.add(divisoria);

	var currentText = 0;

	var btPrev = Titanium.UI.createButton({
		backgroundImage:'images/sobre/btVoltar.png',bottom:5*rh,left:110*rw,width:40*res,height:40*res
	});
	win.add(btPrev);

	btPrev.addEventListener('click',function(e){
		if(currentText == 0){
			currentText = 2;
		}else{
			currentText--;
		}
		refreshText();
	});

	var btNext = Titanium.UI.createButton({
		backgroundImage:'images/sobre/btAvancar.png',bottom:5*rh,left:160*rw,width:40*res,height:40*res
	});
	win.add(btNext);

	btNext.addEventListener('click',function(e){
		if(currentText == 2){
			currentText = 0;
		}else{
			currentText++;
		}
		refreshText();
	});

	function refreshText(){
		switch(currentText){
			case 0:
				txtSobre.setHtml(texto1);
			break;

			case 1:
				txtSobre.setHtml(texto2);
			break;

			case 2:
				txtSobre.setHtml(texto3);
			break;
		}
	}

	return win;
}

module.exports = sobre;