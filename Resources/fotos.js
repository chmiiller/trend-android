function fotos(pHotel) {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: false,
		backgroundColor:'#2e2e2e',
		orientationModes:[Titanium.UI.LANDSCAPE_RIGHT,Titanium.UI.LANDSCAPE_LEFT,Titanium.UI.PORTRAIT],
		fullscreen:true
	});

	var hotel = pHotel;

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'transparent',top:0,left:0,width:Ti.UI.FILL,height:93*res
	});

	var headerTitle = Titanium.UI.createLabel({
		text:'Fotos',color:'white',font:{fontSize:18*res}
	});
	header.add(headerTitle);

	var imageUrl1 = Titanium.App.Properties.mainUrl + hotel.Foto;
	var imageUrl2 = Titanium.App.Properties.mainUrl + hotel.Foto2;
	var imageUrl3 = Titanium.App.Properties.mainUrl + hotel.Foto3;
	var imageUrl4 = Titanium.App.Properties.mainUrl + hotel.Foto4;

	var viewFoto1 = Titanium.UI.createImageView({
		image:imageUrl1,width:Ti.UI.FILL,
	});

	var viewFoto2 = Titanium.UI.createImageView({
		image:imageUrl2,width:Ti.UI.FILL,
	});

	var viewFoto3 = Titanium.UI.createImageView({
		image:imageUrl3,width:Ti.UI.FILL,
	});

	var viewFoto4 = Titanium.UI.createImageView({
		image:imageUrl4,width:Ti.UI.FILL,
	});

	var viewsArray = [viewFoto1,viewFoto2,viewFoto3,viewFoto4];

	//SCROLLABLE
	var fotosContainer = Titanium.UI.createScrollableView({
		backgroundColor:'transparent',top:'15%',left:0,width:Titanium.UI.FILL,height:410*res
	});

	fotosContainer.setViews(viewsArray);
	win.add(fotosContainer);

	win.add(header);

	Titanium.Gesture.addEventListener('orientationchange',function(e){
		if(e.orientation == Titanium.UI.LANDSCAPE_LEFT || e.orientation == Titanium.UI.LANDSCAPE_RIGHT){
			fotosContainer.setTop(0);
			fotosContainer.setHeight(Titanium.UI.FILL);
			fotosContainer.setWidth(568*res);
			viewFoto1.setWidth(568*res);
			viewFoto2.setWidth(568*res);
			viewFoto3.setWidth(568*res);
			viewFoto4.setWidth(568*res);
		}else{
			fotosContainer.setTop('15%');
			fotosContainer.setHeight(410*res);
		}
	});

	return win;
}

module.exports = fotos;