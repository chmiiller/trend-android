function detalhes(pHotel) {
	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var hotel = pHotel;

	var abriuHotel = false;
	var abriuApartamento = false;

	var imageUrl = Titanium.App.Properties.mainUrl + hotel.Logo;

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	var viewStyle = "grade";

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'transparent',top:0,left:0,width:320*res,height:65*res
	});

	var bgHeader = Titanium.UI.createView({
		backgroundColor:'white',opacity:0.8,width:320*res,height:65*res
	});
	header.add(bgHeader);

	var headerTitle = Titanium.UI.createLabel({
		text:hotel.Nome,top:16*rh,
		width:320*res,height:46.5*res,color:'#4783c1',
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
		font:{fontSize:20*rw}
	});
	header.add(headerTitle);

	var mainScroll = Titanium.UI.createScrollView({
		top:0,left:0,width:320*res,height:'auto'
	});
	win.add(mainScroll);

	win.add(header);

	//TITLE
	var titleView = Titanium.UI.createView({
		backgroundImage:'images/lista/tituloCorporativo.png',top:66*rh,left:0,width:320*res,height:50.5*res
	});

	switch(Ti.App.Properties.currentSession){
		case 'corporativo':
			titleView.setBackgroundImage('images/lista/tituloCorporativo.png');
		break;

		case 'internacional':
			titleView.setBackgroundImage('images/lista/tituloCorporativoInternacional.png');
		break;

		case 'lazer':
			titleView.setBackgroundImage('images/lista/tituloLazer.png');
		break;

		case 'eventos':
			titleView.setBackgroundImage('images/lista/tituloEventos.png');
		break;
	}

	mainScroll.add(titleView);

	//LOGO
	var logoContainer = Titanium.UI.createView({
		backgroundColor:'transparent',//f8f8f8
		top:116*rh,left:0,width:150*res,height:70*res
	});
	mainScroll.add(logoContainer);

	var logo = Titanium.UI.createImageView({
		image:imageUrl,
		width:70*res,height:70*res,
		defaultImage:'images/defaultImageLogo.png'
	});
	logoContainer.add(logo);

	//STARS
	var starsContainer = Titanium.UI.createView({
		backgroundColor:'#f8f8f8',top:116*rh,right:0,width:170*res,height:60*res
	});
	mainScroll.add(starsContainer);

	populateStarsNumber(hotel.Estrelas);

	var divisoria = Titanium.UI.createView({
		backgroundImage:'images/detalhes/divisoria.png',top:176*rh,left:0,width:320*res,height:38*res
	});
	mainScroll.add(divisoria);

	//CITY
	var lbCidade = Titanium.UI.createLabel({
		color:'#8d8d8f',text:hotel.Cidade.Nome + ' / ' + hotel.Cidade.Estado.Nome,
		top:187*rh,left:5*rw,font:{fontSize:15*res}
	});
	mainScroll.add(lbCidade);

	//DESCRIPTION
	var textoDescricao = '<font color="#5a5a5a" face="helvetica" size="2">' + hotel.Descricao + '</font>';
	var descricao = Titanium.UI.createWebView({
		html:textoDescricao,
		top:214*rh,left:0,width:320*res,height:162*res,
		backgroundColor:'transparent',
		softKeyboardOnFocus: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS,
		enableZoomControls:false
	});
	mainScroll.add(descricao);

	var divisoria2 = Titanium.UI.createView({
		backgroundImage:'images/detalhes/divisoria2.png',top:367*rh,left:0,width:320*res,height:10*res
	});
	mainScroll.add(divisoria2);

	//LOCATION
	var localizacaoLabel = Titanium.UI.createView({
		backgroundImage:'images/detalhes/localizacao.png',top:378*rh,left:0,width:150*res,height:30*res
	});
	mainScroll.add(localizacaoLabel);

	//MAP
	var Map;
	var latitude = hotel.Latitude;
    var longitude = hotel.Longitude;
    var hotelPin;
	var mapView;

	setTimeout(function(){
		Map = require('ti.map');

	    hotelPin = Map.createAnnotation({
		    latitude:latitude,
		    longitude:longitude,
		    title:hotel.Nome,
		    subtitle:hotel.Endereco + ', ' + hotel.Numero,
		    image:'images/detalhes/pin.png'
		});

		mapView = Map.createView({
			width:Ti.UI.FILL,
			top:410*res,
			height:230*res,
			mapType:Map.NORMAL_TYPE,
			userLocation:true,
			annotations:[hotelPin],
			animate:false,
		});
		mainScroll.add(mapView);

	    mapView.setRegion({
			latitude: latitude,
			longitude: longitude, 
			latitudeDelta:0.01, 
			longitudeDelta:0.01,
		});
	},200);
	
    //TABLE
    var positionTable = (640*res) + 15*rh;
    var table = Titanium.UI.createTableView({
    	backgroundColor:'transparent',top:positionTable,left:0,width:320*res,height:650*res,
    });

    var rowData = [];

    var rowHotel = Titanium.UI.createTableViewRow({
    	backgroundImage:'images/detalhes/rowHotel.png',width:320*res,height:86*res,type:'hotel'
    });
    rowData.push(rowHotel);

    var rowHotelContentFake = Titanium.UI.createTableViewRow({
    	width:320*res,height:1,
    });
    rowData.push(rowHotelContentFake);
    var rowHotelContent = Titanium.UI.createTableViewRow({
    	backgroundImage:'images/detalhes/boxDescricao.png',width:320*res,height:189*res
    });
    var contentRowHotel = Titanium.UI.createTextArea({
		value:hotel.DescricaoHotel,color:'white',
		width:300*res,height:180*res,
		top:10*rh,
		backgroundColor:'transparent',
		font:{fontSize:14*res}
	});
	rowHotelContent.add(contentRowHotel);
	//rowData.push(rowHotelContent);

    var rowApartamento = Titanium.UI.createTableViewRow({
    	backgroundImage:'images/detalhes/rowAp.png',width:320*res,height:82*res,type:'apartamento'
    });
    rowData.push(rowApartamento);

    var rowApartamentoContent = Titanium.UI.createTableViewRow({
    	backgroundImage:'images/detalhes/boxDescricao.png',width:320*res,height:189*res
    });
    var contentRowApartamento = Titanium.UI.createTextArea({
		value:hotel.DescricaoApartamento,color:'white',
		width:300*res,height:180*res,
		top:10*rh,
		backgroundColor:'transparent',
		font:{fontSize:14*res}
	});
	rowApartamentoContent.add(contentRowApartamento);
	//rowData.push(rowApartamentoContent);

    var rowComoChegar = Titanium.UI.createTableViewRow({
    	backgroundImage:'images/detalhes/rowComoChegar.png',width:320*res,height:82.5*res,
    	type:'comoChegar'
    });
    rowData.push(rowComoChegar);

    //PLUS SIGN
    var btInfoRowHotel = Ti.UI.createButton({
    	backgroundImage:'images/detalhes/plus.png',right:45*rw,width:35*res,height:35*res
    });
    //rowHotel.add(btInfoRowHotel);

    var btInfoRowApartamento = Ti.UI.createButton({
    	backgroundImage:'images/detalhes/plus.png',right:45*rw,width:35*res,height:35*res
    });
    //rowApartamento.add(btInfoRowApartamento);

    //PLUS SIGN'S ANIMATIONS
	var animaBt = Ti.UI.createAnimation({
	    transform : Ti.UI.create2DMatrix({rotate:45}),
	    duration : 400,
	});

	var animaBtVolta = Ti.UI.createAnimation({
	    transform : Ti.UI.create2DMatrix({rotate:-90}),
	    duration : 400,
	});

    table.setData(rowData);
    mainScroll.add(table);

    table.addEventListener('click',function(e){
    	if(e.rowData.type == 'hotel'){
    		if(!abriuHotel){
    			table.insertRowAfter(e.index,rowHotelContent);
    			abriuHotel = true;
    		}else{
    			table.deleteRow(e.index+1);
    			abriuHotel = false;
    		}
    	}else if(e.rowData.type == 'apartamento'){
    		if(!abriuApartamento){
    			table.insertRowAfter(e.index,rowApartamentoContent);
    			abriuApartamento = true;
    		}else{
    			table.deleteRow(e.index+1);
    			abriuApartamento = false;
    		}
    	}else if(e.rowData.type == 'comoChegar'){
    		var comoChegarWindow = require('comoChegar');
			var comoChegar = comoChegarWindow(hotel);
			comoChegar.open();
    	}
    });


	function populateStarsNumber(starsCount){
		var posX = 30;
		for(var i = 0; i < starsCount; i++){
			var star = Titanium.UI.createView({
				backgroundImage:'images/detalhes/star.png',left:posX*rw,width:19*res,height:18*res
			});
			posX += 24;
			starsContainer.add(star);
		}
	}

	return win;
}

module.exports = detalhes;