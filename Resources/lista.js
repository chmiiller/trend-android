function lista(type) {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var brieFont = 'Brie Light';
	var yanoneBold = 'Yanone Kaffeesatz';

	var carregouGrade = false;
	var carregouSlide = false;

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });
    
	//TITLE
	var titleView = Titanium.UI.createView({
		backgroundImage:'images/lista/tituloCorporativo.png',top:58*rh,left:0,width:320*res,height:50.5*res
	});
	win.add(titleView);

	var paginasPorLetra = [];
	var paginasPorLetraSlide = [];
	var alfabetoArray = 
	[
		"A","B","C","D","E","F","G","H","I","J","K","L","M",
		"N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
	];
    
	var currentId = 0;
	var hotelArray;
	var hotelArrayLength;

	var file = null;
	var blob = null;
	var readText = null;
	var data = null;

	if(type != 'corporativo'){
		file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "trend.txt");
		if(file.exists()){
			blob = file.read();
			if(blob){
				readText = blob.text;
				data = JSON.parse(readText);
				loadFile();
			}
		}
	}else{
		file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "trend.txt");
		if(file.exists()){
			blob = file.read();
			if(blob){
				readText = blob.text;
				data = JSON.parse(readText);
				hotelArray = data.CorporativoNacional;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'corporativo';
			}
		}
		

		hotelArrayLength = hotelArray.length;
		Titanium.App.Properties.currentSession = 'corporativo';
		titleView.setBackgroundImage('images/lista/tituloCorporativo.png');
		Titanium.App.clicouCorporativo = false;

		file = null;
		blob = null;
		readText = null;
		data = null;
	}

	function loadFile(){
		Titanium.App.Properties.currentSession =  '';
		switch(type){
			case 'internacional':
				hotelArray = data.CorporativoInternacional;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'internacional';
				titleView.setBackgroundImage('images/lista/tituloCorporativoInternacional.png');
			break;

			case 'lazer':
				hotelArray = data.Lazer;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'lazer';
				titleView.setBackgroundImage('images/lista/tituloLazer.png');
			break;

			case 'eventos':
				hotelArray = data.Eventos;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'eventos';
				titleView.setBackgroundImage('images/lista/tituloEventos.png');
			break;

			case 'receptivo':
				//hotelArray = data.ReceptivoNacional;
				hotelArray = data.CorporativoNacional;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'corporativo';
			break;

			case 'central':
				//hotelArray = data.Locadora;
				hotelArray = data.CorporativoNacional;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'corporativo';
			break;

			case 'feiras':
				//hotelArray = data.Feira;
				hotelArray = data.CorporativoNacional;
				hotelArrayLength = hotelArray.length;
				Titanium.App.Properties.currentSession = 'corporativo';
			break;
		}
		file = null;
		blob = null;
		readText = null;
	}

	var viewStyle = "grade";

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'white',top:0,left:0,width:320*res,height:65*res
	});

	win.add(header);

	var logoTrend = Titanium.UI.createView({
		backgroundImage:'images/trendLogo.png',top:23*rh,left:40*rw,width:79*res,height:32*res
	});
	header.add(logoTrend);

	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSobre.png',top:21*rh,left:160*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSobre);

	btSobre.addEventListener('click',function(e){
		var sobreWindow = require('sobre');
		var sobre = sobreWindow();
		sobre.open();
	});

	var btContato = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btContato.png',top:21*rh,left:206*rw,width:30.5*res,height:30.5*res
	});
	header.add(btContato);

	btContato.addEventListener('click',function(e){
		var contatoWindow = require('contato');
		var contato = contatoWindow();
		contato.open();
	});
	
	var btSearch = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSearch.png',top:21*rh,left:250*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSearch);

	btSearch.addEventListener('click',function(e){
		var filtroWindow = require('filtro');
		var filtro = filtroWindow();
		filtro.open();
	});

	//TOP BUTTONS
	var topContPos = titleView.getTop() + titleView.getHeight();
	var topButtonsContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:topContPos,left:0,width:Ti.UI.FILL,height:46.5*res
	});

	win.add(topButtonsContainer);

	var btLista = Titanium.UI.createButton({
		backgroundImage:'images/lista/btListaOver.jpg',top:0,left:0,width:160*res,height:47*res,data:'lista',
		backgroundSelectedImage:'images/lista/btListaOver.jpg'
	});
	topButtonsContainer.add(btLista);

	/*
	var btGrade = Titanium.UI.createButton({
		backgroundImage:'images/lista/btGrade.png',top:0,left:108*rw,width:107*res,height:46.5*res,data:'grade',
		backgroundSelectedImage:'images/lista/btGradeOver.png'
	});
	topButtonsContainer.add(btGrade);
	*/

	var btSlide = Titanium.UI.createButton({
		backgroundImage:'images/lista/btSlide.jpg',top:0,right:0,width:160*res,height:47*res,data:'slide',
		backgroundSelectedImage:'images/lista/btSlideOver.jpg'
	});
	topButtonsContainer.add(btSlide);

	btLista.addEventListener('click',handleTopButtons);
	//btGrade.addEventListener('click',handleTopButtons);
	btSlide.addEventListener('click',handleTopButtons);

	function handleTopButtons(e){
		switch(e.source.data){
			case 'lista':
				btLista.setBackgroundImage('images/lista/btListaOver.jpg');
				//btGrade.setBackgroundImage('images/lista/btGrade.png');
				btSlide.setBackgroundImage('images/lista/btSlide.jpg');
				if(hotelArrayLength > 0){
					populateLista();
				}
			break;

			case 'grade':
				btLista.setBackgroundImage('images/lista/btLista.jpg');
				//btGrade.setBackgroundImage('images/lista/btGradeOver.png');
				btSlide.setBackgroundImage('images/lista/btSlide.jpg');
				if(!carregouGrade){
					//populateFirstGrade();
					carregouGrade = true;
				}else{
					//populateGrade();
				}
			break;

			case 'slide':
				btLista.setBackgroundImage('images/lista/btLista.jpg');
				//btGrade.setBackgroundImage('images/lista/btGrade.png');
				btSlide.setBackgroundImage('images/lista/btSlideOver.jpg');
				if(hotelArrayLength > 0){
					if(!carregouSlide){
						populateFirstSlide();
						carregouSlide = true;
					}else{
						populateSlide();
					}
				}
			break;
		}
	}

	var viewGradeContainer = Titanium.UI.createScrollableView({
		backgroundColor:'white',top:145*rh,left:0,width:320*res,height:410*res
	});

	var viewSlideContainer = Titanium.UI.createScrollableView({
		backgroundColor:'white',top:145*rh,left:0,width:320*rw,height:410*rh
	});

	var viewListaContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:145*rh,left:0,width:320*rw,height:410*rh
	});

	var rowDataLista = [];
	var tableLista = Titanium.UI.createTableView({
		backgroundColor:'#f7f7f7',
	});
	viewListaContainer.add(tableLista);

	tableLista.addEventListener('click',function(e){
		var _id = e.rowData.id;
		toggleMenu(_id);
	});

	var activityIndicator = Ti.UI.createActivityIndicator({
		color: '#717171',
	  	font: {fontFamily:'Helvetica Neue', fontSize:18*rw},
		message: ' Carregando...',
	});
	activityIndicator.show();

	setTimeout(function(e){
		if(hotelArray.length > 0){
			addScrollables();
		}else{
			var lbSemResultados = Ti.UI.createLabel({
				color: '#717171',
			  	font: {fontFamily:'Helvetica Neue', fontSize:19*res},
				text:'Nenhum Resultado Encontrado'
			});
			activityIndicator.hide();
			win.add(lbSemResultados);
		}
	}, 500);

	var scrollLetras = Titanium.UI.createScrollView({
		backgroundColor:'#ffffff',bottom:0,height:41*res,width:Ti.UI.FILL,layout:'horizontal',
		horizontalWrap:false,scrollType:'horizontal'
	});

	var scrollLetrasSlide = Titanium.UI.createScrollView({
		backgroundColor:'#ffffff',bottom:0,height:41*res,width:Ti.UI.FILL,layout:'horizontal',
		scrollType:'horizontal',horizontalWrap:false
	});

	var currentButtonLetraGrade = null;
	var currentButtonLetra = null;

	function addScrollables(){
		win.add(menuView);
		viewListaContainer.hide();
		populateFirstLista();
		activityIndicator.hide();
	}

	function populateLetrasScroll(){
		for(var i = 0; i < paginasPorLetra.length; i++){
			var _item = paginasPorLetra[i];
			var bt = Titanium.UI.createView({
				backgroundImage:'images/lista/letraContainer.jpg',width:54*res,height:41*res,
				dataLetra:_item.letra,dataPagina:_item.pagina
			});
			bt.addEventListener('click',function(e){
				//resetCurrentButtonGrade();
				viewGradeContainer.scrollToView(e.source.dataPagina);
				//e.source.letraLabel.setColor('orange');
				//currentButtonLetraGrade = e.source;
			});
			var lb = Titanium.UI.createLabel({
				text:_item.letra,color:'#babcc0',
				font:{fontFamily:yanoneBold,fontSize:18*res},
				dataPagina:_item.pagina
			});
			lb.addEventListener('click',function(e){
				viewGradeContainer.scrollToView(e.source.dataPagina);
			});
			bt.letraLabel = lb;
			bt.add(lb);
			scrollLetras.add(bt);
		}
		//win.add(scrollLetras);
	}

	function populateLetrasScrollSlide(){
		for(var i = 0; i < paginasPorLetraSlide.length; i++){
			var _item = paginasPorLetraSlide[i];
			var bt = Titanium.UI.createView({
				backgroundImage:'images/lista/letraContainer.jpg',width:54*res,height:41*res,
				dataLetra:_item.letra,dataPagina:_item.pagina
			});
			bt.addEventListener('click',function(e){
				//resetCurrentButton();
				refreshSlideToView(e.source.dataPagina);
				//viewSlideContainer.scrollToView(e.source.dataPagina);
				//e.source.letraLabel.setColor('orange');
				//currentButtonLetra = e.source;
			});
			var lb = Titanium.UI.createLabel({
				text:_item.letra,color:'#babcc0',
				font:{fontFamily:yanoneBold,fontSize:18*res},
				dataPagina:_item.pagina
			});
			lb.addEventListener('click',function(e){
				viewGradeContainer.scrollToView(e.source.dataPagina);
			});
			bt.letraLabel = lb;
			bt.add(lb);
			scrollLetrasSlide.add(bt);
		}
		scrollLetrasSlide.hide();
		win.add(scrollLetrasSlide);
	}

	function resetCurrentButton(){
		if(currentButtonLetra){
			//currentButtonLetra.letraLabel.setColor('#babcc0');
		}
	}

	function resetCurrentButtonGrade(){
		if(currentButtonLetraGrade){
			//currentButtonLetraGrade.letraLabel.setColor('#babcc0');
		}
	}

	function populateGrade(){
		if(viewStyle == 'slide' || viewStyle == 'lista'){
			scrollLetrasSlide.hide();
			scrollLetras.show();
			viewSlideContainer.hide();
			viewListaContainer.hide();
		}
		
		viewStyle = 'grade';
		viewGradeContainer.show();
	}

	function populateFirstGrade(){
		scrollLetrasSlide.hide();
		viewSlideContainer.hide();
		viewListaContainer.hide();
		activityIndicator.show();

		var currentLetra = '';
		var currentLetraSlide = '';
		var viewsArray = [];

		var percent = 0;
		for(var i = 0; i < hotelArrayLength; i++){
			percent = Math.round((i * 100)/hotelArrayLength);
			activityIndicator.setMessage('Carregando: ' + percent + '%');
			var index = i % 4;
			if(index == 0){
				var viewContainer = Titanium.UI.createView({width:320*rw,height:410*rh});
				viewsArray.push(viewContainer);
			}
			var itemGrade = require('gradeObj');
			var hotel = hotelArray[i];

			var primeiraLetra = hotel.Nome.charAt(0);
			if(primeiraLetra == 'Á' || primeiraLetra == 'a'){
				primeiraLetra = 'A';
			}else if(primeiraLetra == 'Í' || primeiraLetra == 'i'){
				primeiraLetra = 'I';
			}else if(primeiraLetra == 'É' || primeiraLetra == 'e'){
				primeiraLetra = 'E';
			}
			if(primeiraLetra != currentLetra){
				var _pagina = viewsArray.length-1;
				var _obj = {letra:primeiraLetra,pagina:_pagina};
				currentLetra = primeiraLetra;
				paginasPorLetra.push(_obj);
			}


			var item = itemGrade(index,toggleMenu,hotel);
			viewsArray[viewsArray.length-1].add(item);
		}
		viewGradeContainer.setViews(viewsArray);
		//win.add(viewGradeContainer);
		populateLetrasScroll();
		scrollLetras.show();
		activityIndicator.hide();
		viewStyle = 'grade';
		viewGradeContainer.show();
	}

	function populateFirstSlide(){
		scrollLetras.hide();
		viewListaContainer.hide();
		viewGradeContainer.hide();

		var currentLetra = '';
		var currentLetraSlide = '';
		var viewsArraySlide = [];
		var percent = 0;

		activityIndicator.show();

		for(var i = 0; i < 7; i++){
			percent = Math.round((i * 100)/hotelArrayLength);
			activityIndicator.setMessage('Carregando: ' + percent + '%');

			var viewContainerSlide = Titanium.UI.createView({width:320*rw,height:410*rh});
			viewsArraySlide.push(viewContainerSlide);
			var itemSlide = require('slideObj');
			var hotel = hotelArray[i];

			var itemNovoSlide = itemSlide(toggleMenu,hotel);
			viewContainerSlide.add(itemNovoSlide);
		}
		viewSlideContainer.setViews(viewsArraySlide);

		for(var j = 0; j < hotelArrayLength; j++){
			var hotel = hotelArray[j];
			var primeiraLetraSlide = hotel.Nome.charAt(0);
			if(primeiraLetraSlide == 'Á' || primeiraLetraSlide == 'a'){
				primeiraLetraSlide = 'A';
			}else if(primeiraLetraSlide == 'Í' || primeiraLetraSlide == 'i'){
				primeiraLetraSlide = 'I';
			}else if(primeiraLetraSlide == 'É' || primeiraLetraSlide == 'e'){
				primeiraLetraSlide = 'E';
			}
			if(primeiraLetraSlide != currentLetraSlide){
				var _pagina = j;
				var _obj = {letra:primeiraLetraSlide,pagina:_pagina};
				currentLetraSlide = primeiraLetraSlide;
				paginasPorLetraSlide.push(_obj);
			}
		}

		win.add(viewSlideContainer);
		activityIndicator.hide();
		populateLetrasScrollSlide();
		scrollLetrasSlide.show();
		viewStyle = 'slide';
		viewSlideContainer.show();

		viewSlideContainer.addEventListener('scrollend',draggableEnd);
	}

	//
	//SCROLLABLE FUNCTIONS
	//
	var currentIndex = 0;
	var atualPage = 0;
	var fromSetCurrentPage = 0;

	function refreshSlideToView(pNumber){
		var newViewsArraySlide = [];
		if(pNumber < (hotelArrayLength-6)){
			for(var i = pNumber; i < (pNumber+6); i++){
				var viewContainerSlide = Titanium.UI.createView({width:320*rw,height:410*rh});
				newViewsArraySlide.push(viewContainerSlide);
				var itemSlide = require('slideObj');
				var hotel = hotelArray[i];

				var itemNovoSlide = itemSlide(toggleMenu,hotel);
				viewContainerSlide.add(itemNovoSlide);
			}
		}else{
			for(var i = pNumber; i < hotelArrayLength; i++){
				var viewContainerSlide = Titanium.UI.createView({width:320*rw,height:410*rh});
				newViewsArraySlide.push(viewContainerSlide);
				var itemSlide = require('slideObj');
				var hotel = hotelArray[i];

				var itemNovoSlide = itemSlide(toggleMenu,hotel);
				viewContainerSlide.add(itemNovoSlide);
			}
		}
		viewSlideContainer.setViews(newViewsArraySlide);
		viewSlideContainer.setCurrentPage(0);

		currentIndex = pNumber;
		atualPage = 0;
		fromSetCurrentPage = 1;
	}

	function draggableEnd(e){
		if(fromSetCurrentPage == 0){
			if(viewSlideContainer.currentPage < atualPage){
				//Voltando
				currentIndex--;
				if(viewSlideContainer.currentPage == 0){
					refreshArrayPrev();
				}
				atualPage = viewSlideContainer.currentPage;
			}else if(viewSlideContainer.currentPage > atualPage){
				//Indo
				currentIndex++;
				if(viewSlideContainer.currentPage == (viewSlideContainer.views.length)-4){
					refreshArrayNext();
				}
				atualPage = viewSlideContainer.currentPage;
			}
		}

		if(fromSetCurrentPage == 2){
			fromSetCurrentPage = 0;
		}

		if(fromSetCurrentPage == 1){
			fromSetCurrentPage = 2;
		}
	}

	function refreshArrayPrev(){
		
		var newArray = viewSlideContainer.getViews();
		newArray.pop();
		newArray.pop();
		
		if(currentIndex != 0 && currentIndex != 1){

			var viewContainerSlide = Titanium.UI.createView({width:320*rw,height:410*rh});
			var itemSlide = require('slideObj');
			var hotel = hotelArray[currentIndex-1];
			var itemNovoSlide = itemSlide(toggleMenu,hotel);
			viewContainerSlide.add(itemNovoSlide);

			var viewContainerSlide2 = Titanium.UI.createView({width:320*rw,height:410*rh});
			var itemSlide2 = require('slideObj');
			var hotel2 = hotelArray[currentIndex-2];
			var itemNovoSlide2 = itemSlide2(toggleMenu,hotel2);
			viewContainerSlide2.add(itemNovoSlide2);

			newArray.unshift(viewContainerSlide2,viewContainerSlide);
			viewSlideContainer.setViews(newArray);
			fromSetCurrentPage = 1;
			currentIndex -= 2;
			viewSlideContainer.setCurrentPage(viewSlideContainer.currentPage+2);
		}
	}

	function refreshArrayNext(){
		var newArray = viewSlideContainer.getViews();
		newArray.splice(0,2);
		if(currentIndex != hotelArrayLength && currentIndex != hotelArrayLength-1){
			if(hotelArray[currentIndex+4]){
				var viewContainerSlide = Titanium.UI.createView({width:320*rw,height:410*rh});
				var itemSlide = require('slideObj');
				var hotel = hotelArray[currentIndex+4];
				var itemNovoSlide = itemSlide(toggleMenu,hotel);
				viewContainerSlide.add(itemNovoSlide);

				newArray.push(viewContainerSlide);
			}
			
			if(hotelArray[currentIndex+5]){
				var viewContainerSlide2 = Titanium.UI.createView({width:320*rw,height:410*rh});
				var itemSlide2 = require('slideObj');
				var hotel2 = hotelArray[currentIndex+5];
				var itemNovoSlide2 = itemSlide2(toggleMenu,hotel2);
				viewContainerSlide2.add(itemNovoSlide2);

				newArray.push(viewContainerSlide2);
			}
			viewSlideContainer.setViews(newArray);
			fromSetCurrentPage = 1;
			currentIndex += 2;
			viewSlideContainer.setCurrentPage(viewSlideContainer.currentPage-2);
		}
	}

	//
	//END OF SCROLLABLE MANAGEMENT FUNCTIONS

	function populateSlide(){
		if(viewStyle == 'grade' || viewStyle == 'lista'){
			scrollLetrasSlide.show();
			scrollLetras.hide();
			viewListaContainer.hide();
			viewGradeContainer.hide();
		}
		viewStyle = 'slide';
		viewSlideContainer.show();
	}

	function populateLista(){
		tableLista.setData(rowDataLista);

		if(viewStyle == 'grade' || viewStyle == 'slide'){
			scrollLetrasSlide.hide();
			scrollLetras.hide();
			viewGradeContainer.hide();
			viewSlideContainer.hide();
		}

		viewStyle = 'lista';
		viewListaContainer.show();
	}

	var numberOfRows = hotelArrayLength;
	var showRows = 50;
	var rowNumber = 1;

	function populateFirstLista(){
		var percent = 0;
		var totalLength = 50;
		if(hotelArrayLength <= 50){
			totalLength = hotelArrayLength;
		}
		for(var i = 0; i < totalLength; i++){
			percent = Math.round((i * 100)/hotelArrayLength);
			activityIndicator.setMessage('Carregando: ' + percent + '%');
			var _item = hotelArray[i];
			var row = Titanium.UI.createTableViewRow({
				backgroundColor:'#f7f7f7',height:137*rh,
				id:_item.Id
			});
			
			var lblName = Titanium.UI.createLabel({
				text:hotelArray[i].Nome,color:'#5270ae',top:15*rh,font:{fontSize:17*rw,fontWeight:'bold'}
			});

			var valuesContainer = Titanium.UI.createView({
				backgroundImage:'images/lista/rowTabela.png',top:55*rh,left:0,width:320*res,height:82*res
			});

			var lbCat = Titanium.UI.createLabel({
				text:_item.CategoriaHotel.Id,color:'#5270ae',top:35*rh,left:40*rw,
				font:{fontSize:17*rw,fontWeight:'bold',fontFamily:yanoneBold}
			});
			valuesContainer.add(lbCat);

			var lbSgl = Titanium.UI.createLabel({
				text:'SGL - STD',color:'#5270ae',top:30*rh,left:111*rw,
				font:{fontSize:14*rw,fontWeight:'bold',fontFamily:yanoneBold}
			});
			valuesContainer.add(lbSgl);

			var lbDbl = Titanium.UI.createLabel({
				text:'DBL - STD',color:'#5270ae',bottom:4*rh,left:111*rw,
				font:{fontSize:14*rw,fontWeight:'bold',fontFamily:yanoneBold}
			});
			valuesContainer.add(lbDbl);

			var lbBalcaoSgl = Titanium.UI.createLabel({
				color:'#5270ae',top:33*rh,left:190*rw,width:65*res,height:20*res,
				font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
				textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
			});
			if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoBalcao){
				lbBalcaoSgl.setText(_item.Quartos[0].Precos[0].PrecoBalcao);
			}else{
				lbBalcaoSgl.setText('sob consulta');
				lbBalcaoSgl.setFont({fontSize:7*res,fontFamily:yanoneBold});
			}
			valuesContainer.add(lbBalcaoSgl);

			var lbBalcaoDbl = Titanium.UI.createLabel({
				color:'#5270ae',bottom:4*rh,left:190*rw,width:65*res,height:20*res,
				font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
				textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
			});
			if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoBalcaoDBL){
				lbBalcaoDbl.setText(_item.Quartos[0].Precos[0].PrecoBalcaoDBL);
			}else{
				lbBalcaoDbl.setText('sob consulta');
				lbBalcaoDbl.setFont({fontSize:7*res,fontFamily:yanoneBold});
			}
			valuesContainer.add(lbBalcaoDbl);

			var lbAcordoSgl = Titanium.UI.createLabel({
				color:'#5270ae',top:33*rh,left:258*rw,width:65*res,height:20*res,
				font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
				textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
			});
			if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoAcordo){
				lbAcordoSgl.setText(_item.Quartos[0].Precos[0].PrecoAcordo);
			}else{
				lbAcordoSgl.setText('sob consulta');
				lbAcordoSgl.setFont({fontSize:7*res,fontFamily:yanoneBold});
			}
			valuesContainer.add(lbAcordoSgl);

			var lbAcordoDbl = Titanium.UI.createLabel({
				color:'#5270ae',bottom:4*rh,left:258*rw,width:65*res,height:20*res,
				font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
				textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
			});
			if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoAcordoDBL){
				lbAcordoDbl.setText(_item.Quartos[0].Precos[0].PrecoAcordoDBL);
			}else{
				lbAcordoDbl.setText('sob consulta');
				lbAcordoDbl.setFont({fontSize:7*res,fontFamily:yanoneBold});
			}
			valuesContainer.add(lbAcordoDbl);

			row.add(lblName);
			row.add(valuesContainer);
			rowDataLista.push(row);

			rowNumber++;
		}
		tableLista.setData(rowDataLista);

		tableLista.addEventListener('scroll',function(e){
			if(type != 'eventos'){
				if((e.firstVisibleItem+e.visibleItemCount) == e.totalItemCount){
					for (var c = 0; c < showRows; c++){
						if(rowNumber == numberOfRows){return;}

						var _item = hotelArray[rowNumber];
						var row = Titanium.UI.createTableViewRow({
							backgroundColor:'#f7f7f7',height:137*rh,busca:_item.Nome,
							id:_item.Id
						});
						var lblName = Titanium.UI.createLabel({
							text:_item.Nome,color:'#5270ae',top:15*rh,font:{fontSize:17*rw,fontWeight:'bold'}
						});

						var valuesContainer = Titanium.UI.createView({
							backgroundImage:'images/lista/rowTabela.png',top:55*rh,left:0,width:320*res,height:82*res
						});

						var lbCat = Titanium.UI.createLabel({
							text:_item.CategoriaHotel.Id,color:'#5270ae',top:35*rh,left:40*rw,
							font:{fontSize:17*rw,fontWeight:'bold',fontFamily:yanoneBold}
						});
						valuesContainer.add(lbCat);

						var lbSgl = Titanium.UI.createLabel({
							text:'SGL - STD',color:'#5270ae',top:30*rh,left:111*rw,
							font:{fontSize:14*rw,fontWeight:'bold',fontFamily:yanoneBold}
						});
						valuesContainer.add(lbSgl);

						var lbDbl = Titanium.UI.createLabel({
							text:'DBL - STD',color:'#5270ae',bottom:4*rh,left:111*rw,
							font:{fontSize:14*rw,fontWeight:'bold',fontFamily:yanoneBold}
						});
						valuesContainer.add(lbDbl);

						var lbBalcaoSgl = Titanium.UI.createLabel({
							color:'#5270ae',top:33*rh,left:190*rw,width:65*res,height:20*res,
							font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
							textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
						});
						if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoBalcao){
							lbBalcaoSgl.setText(_item.Quartos[0].Precos[0].PrecoBalcao);
						}else{
							lbBalcaoSgl.setText('sob consulta');
							lbBalcaoSgl.setFont({fontSize:7*res,fontFamily:yanoneBold});
						}
						valuesContainer.add(lbBalcaoSgl);

						var lbBalcaoDbl = Titanium.UI.createLabel({
							color:'#5270ae',bottom:4*rh,left:190*rw,width:65*res,height:20*res,
							font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
							textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
						});
						if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoBalcaoDBL){
							lbBalcaoDbl.setText(_item.Quartos[0].Precos[0].PrecoBalcaoDBL);
						}else{
							lbBalcaoDbl.setText('sob consulta');
							lbBalcaoDbl.setFont({fontSize:7*res,fontFamily:yanoneBold});
						}
						valuesContainer.add(lbBalcaoDbl);

						var lbAcordoSgl = Titanium.UI.createLabel({
							color:'#5270ae',top:33*rh,left:258*rw,width:65*res,height:20*res,
							font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
							textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
						});
						if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoAcordo){
							lbAcordoSgl.setText(_item.Quartos[0].Precos[0].PrecoAcordo);
						}else{
							lbAcordoSgl.setText('sob consulta');
							lbAcordoSgl.setFont({fontSize:7*res,fontFamily:yanoneBold});
						}
						valuesContainer.add(lbAcordoSgl);

						var lbAcordoDbl = Titanium.UI.createLabel({
							color:'#5270ae',bottom:4*rh,left:258*rw,width:65*res,height:20*res,
							font:{fontSize:11*rw,fontWeight:'bold',fontFamily:yanoneBold},
							textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
						});
						if(_item.Quartos[0] && _item.Quartos[0].Precos[0].PrecoAcordoDBL){
							lbAcordoDbl.setText(_item.Quartos[0].Precos[0].PrecoAcordoDBL);
						}else{
							lbAcordoDbl.setText('sob consulta');
							lbAcordoDbl.setFont({fontSize:7*res,fontFamily:yanoneBold});
						}
						valuesContainer.add(lbAcordoDbl);

						row.add(lblName);
						row.add(valuesContainer);

						tableLista.appendRow(row,{});
						rowNumber++;
					}
				}
			}
		});
		
		scrollLetrasSlide.hide();
		scrollLetras.hide();
		viewGradeContainer.hide();
		viewSlideContainer.hide();
		viewStyle = 'lista';
		viewListaContainer.show();
	}

	//MENU VIEW
	var menuView = Titanium.UI.createView({
		backgroundColor:'transparent',left:0,width:320*rw,height:236.5*rh,bottom:-240*rh,
		visible:false,toggle:false,zIndex:100
	});

	var menuViewBg = Titanium.UI.createView({
		backgroundImage:'images/lista/bgMenu.png',width:320*rw,height:236.5*rh,opacity:0.9
	});
	menuView.add(menuViewBg);

	var btInfo = Titanium.UI.createButton({
		backgroundImage:'images/lista/btMenuInfo.png',top:20*rh,left:50*rw,width:58*res,height:76*res
	});
	menuView.add(btInfo);

	btInfo.addEventListener('click',function(e){
		for(var i = 0; i < hotelArrayLength; i++){
			var _item = hotelArray[i];
			if(_item.Id == currentId){
				hotel = _item;
				toggleMenu();
				var detalhesWindow = require('detalhes');
				var detalhes = detalhesWindow(hotel);
				detalhes.open();
			}
		}
	});

	var btComo = Titanium.UI.createButton({
		backgroundImage:'images/lista/btMenuComoChegar.png',top:20*rh,right:50*rw,width:58.5*res,height:74.5*res
	});
	menuView.add(btComo);

	btComo.addEventListener('click',function(e){
		for(var i = 0; i < hotelArrayLength; i++){
			var _item = hotelArray[i];
			if(_item.Id == currentId){
				hotel = _item;
				toggleMenu();
				var comoChegarWindow = require('comoChegar');
				var comoChegar = comoChegarWindow(hotel);
				comoChegar.open();
			}
		}
	});

	var btFotos = Titanium.UI.createButton({
		backgroundImage:'images/lista/btMenuFotos.png',bottom:20*rh,left:47*rw,width:70*res,height:71*res
	});
	menuView.add(btFotos);

	btFotos.addEventListener('click',function(e){
		for(var i = 0; i < hotelArrayLength; i++){
			var _item = hotelArray[i];
			if(_item.Id == currentId){
				hotel = _item;
				toggleMenu();
				var fotosWindow = require('fotos');
				var fotos = fotosWindow(hotel);
				fotos.open();
			}
		}
	});

	var btTarifas = Titanium.UI.createButton({
		backgroundImage:'images/lista/btMenuTarifas.png',bottom:20*rh,right:40*rw,width:68*res,height:73.5*res
	});
	menuView.add(btTarifas);

	btTarifas.addEventListener('click',function(e){
		for(var i = 0; i < hotelArrayLength; i++){
			var _item = hotelArray[i];
			if(_item.Id == currentId){
				hotel = _item;
				toggleMenu();
				var tarifasWindow = require('tarifas');
				var tarifas = tarifasWindow(hotel);
				tarifas.open();
			}
		}
	});

	function toggleMenu(id){
		if(menuView.toggle == true){
			var animaHideMenu = Titanium.UI.createAnimation({
				duration:200,bottom:-240*rh,
			});
			//menuView.hide();
			menuView.toggle = false;
			menuView.animate(animaHideMenu);
		}else{
			var animaShowMenu = Titanium.UI.createAnimation({
				duration:200,bottom:0,
			});
			currentId = id;
			menuView.show();
			menuView.toggle = true;
			menuView.animate(animaShowMenu);
		}
	}

	win.addEventListener('android:back', function(e){
        if(menuView.toggle == true){
			var animaHideMenu = Titanium.UI.createAnimation({
				duration:200,bottom:-240*rh,
			});
			menuView.toggle = false;
			menuView.animate(animaHideMenu);
		}else{
			win.close();
		}
    });

	win.add(viewListaContainer);
	win.add(activityIndicator);

	return win;
}

module.exports = lista;