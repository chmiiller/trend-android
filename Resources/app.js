var win = Titanium.UI.createWindow({
    backgroundColor:'black',
    navBarHidden:true,
    fullscreen: true,
});


var deviceHeight = Ti.Platform.displayCaps.platformHeight;
var deviceWidth = Ti.Platform.displayCaps.platformWidth;

var widthScale = (deviceWidth / 320);
var heightScale = (deviceHeight / 480);
Titanium.App.rw = widthScale;
Titanium.App.rh = heightScale;

if(Titanium.App.rw < Titanium.App.rh){
	Titanium.App.res = Titanium.App.rw;
}else{
	Titanium.App.res = Titanium.App.rh;
}

var countly = require("ly.count");

var mainUrl = 'http://54.207.16.197/DiretorioTrend/Upload/';
Titanium.App.Properties.mainUrl = mainUrl;

if(Titanium.Network.online){
	countly.countInit("https://cloud.count.ly","151ed5a7c3ab1da69e6cf2d7c33ea51a1846940b");
}

var homeWindow = require('home');
var home = homeWindow();
home.open();