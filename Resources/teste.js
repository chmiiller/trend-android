function teste(){
	var win = Titanium.UI.createWindow({
	    backgroundColor:'#d0d0d0',
	    navBarHidden: true,
	});

	var currentIndex = 0;
	var atualPage = 0;
	var fromSetCurrentPage = 0;

	var scrollable = Ti.UI.createScrollableView({
		backgroundColor:'white',top:80,width:300,height:400
	});
	win.add(scrollable);
	scrollable.addEventListener('scrollend',draggableEnd);

	var scrollableArray = [];
	var fakeData = [
		{width:200,height:200,backgroundColor:'green',id:0},
		{width:200,height:200,backgroundColor:'blue',id:1},
		{width:200,height:200,backgroundColor:'red',id:2},
		{width:200,height:200,backgroundColor:'black',id:3},
		{width:200,height:200,backgroundColor:'gray',id:4},
		{width:200,height:200,backgroundColor:'pink',id:5},
		{width:200,height:200,backgroundColor:'aqua',id:6},
		{width:200,height:200,backgroundColor:'brown',id:7},
		{width:200,height:200,backgroundColor:'darkgray',id:9},
		{width:200,height:200,backgroundColor:'fuchsia',id:10},
		{width:200,height:200,backgroundColor:'lightgray',id:11},
		{width:200,height:200,backgroundColor:'lime',id:12},
		{width:200,height:200,backgroundColor:'magenta',id:13},
		{width:200,height:200,backgroundColor:'maroon',id:14},
		{width:200,height:200,backgroundColor:'navy',id:15},
		{width:200,height:200,backgroundColor:'olive',id:16},
		{width:200,height:200,backgroundColor:'orange',id:17},
		{width:200,height:200,backgroundColor:'purple',id:18},
		{width:200,height:200,backgroundColor:'silver',id:19},
		{width:200,height:200,backgroundColor:'teal',id:20},
		{width:200,height:200,backgroundColor:'yellow',id:21},
		{width:200,height:200,backgroundColor:'blue',id:22},
		{width:200,height:200,backgroundColor:'red',id:23},
		{width:200,height:200,backgroundColor:'black',id:24},
		{width:200,height:200,backgroundColor:'gray',id:25},
		{width:200,height:200,backgroundColor:'pink',id:26},
		{width:200,height:200,backgroundColor:'aqua',id:27},
	];

	for(var i = 0; i < 7; i++){
		var item = fakeData[i];
		var view = Ti.UI.createView({
			width:item.width,height:item.height,backgroundColor:item.backgroundColor,id:item.id,borderColor:'black',
		});
		scrollableArray.push(view);
	}
	scrollable.setViews(scrollableArray);

	function draggableEnd(e){
		//SE VIER DO setCurrentPage() não deve rodar
		if(fromSetCurrentPage == 0){
			if(scrollable.currentPage < atualPage){
				//Voltando
				currentIndex--;
				if(scrollable.currentPage == 0){
					refreshArrayPrev();
				}
				atualPage = scrollable.currentPage;
			}else if(scrollable.currentPage > atualPage){
				//Indo
				currentIndex++;
				if(scrollable.currentPage == (scrollable.views.length)-4){
					refreshArrayNext();
				}
				atualPage = scrollable.currentPage;
			}
		}

		if(fromSetCurrentPage == 2){
			fromSetCurrentPage = 0;
		}

		if(fromSetCurrentPage == 1){
			fromSetCurrentPage = 2;
		}
	}

	function refreshArrayPrev(){
		var newArray = scrollable.getViews();
		if(currentIndex != 0 && currentIndex != 1){
			var v1 = Ti.UI.createView({
				width:fakeData[currentIndex-1].width,height:fakeData[currentIndex-1].height,
				backgroundColor:fakeData[currentIndex-1].backgroundColor,id:fakeData[currentIndex-1].id,borderColor:'black'
			});

			var v2 = Ti.UI.createView({
				width:fakeData[currentIndex-2].width,height:fakeData[currentIndex-2].height,
				backgroundColor:fakeData[currentIndex-2].backgroundColor,id:fakeData[currentIndex-2].id,borderColor:'black'
			});
			newArray.unshift(v2,v1);
			newArray.pop();
			newArray.pop();
			scrollable.setViews(newArray);
			fromSetCurrentPage = 1;
			currentIndex -= 2;
			scrollable.setCurrentPage(scrollable.currentPage+2);
		}
	}

	function refreshArrayNext(){
		var newArray = scrollable.getViews();
		newArray.splice(0,2);
		if(currentIndex != fakeData.length && currentIndex != (fakeData.length)-1){
			if(fakeData[currentIndex+4]){
				var v1 = Ti.UI.createView({
					width:fakeData[currentIndex+4].width,height:fakeData[currentIndex+4].height,
					backgroundColor:fakeData[currentIndex+4].backgroundColor,id:fakeData[currentIndex+4].id,borderColor:'black'
				});
				newArray.push(v1);
			}
			
			if(fakeData[currentIndex+5]){
				var v2 = Ti.UI.createView({
					width:fakeData[currentIndex+5].width,height:fakeData[currentIndex+5].height,
					backgroundColor:fakeData[currentIndex+5].backgroundColor,id:fakeData[currentIndex+5].id,borderColor:'black'
				});
				newArray.push(v2);
			}
			scrollable.setViews(newArray);
			fromSetCurrentPage = 1;
			currentIndex += 2;
			scrollable.setCurrentPage(scrollable.currentPage-2);
		}
	}

	win.addEventListener('android:back', function(e){
		scrollable.removeEventListener('scrollend',draggableEnd);
		win.close();
	});

	return win;
}

module.exports = teste;