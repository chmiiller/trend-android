function contato() {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		backgroundColor:'#276385',
		navBarHidden: true,
		fullscreen: true,
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });
	
	//HEADERS AND SHOULDERS
	var header = Titanium.UI.createView({
		backgroundColor:'white',top:0,left:0,width:320*res,height:70*res,
	});
	win.add(header);

	var headerTitle = Titanium.UI.createLabel({
		text:'Onde Estamos',top:16*rh,
		width:320*res,height:46.5*res,color:'#4783c1',
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
		font:{fontSize:20*res}
	});

	var scroll = Titanium.UI.createScrollView({
		top:71*rh,left:0,width:Ti.UI.FILL,height:'auto'
	});
	win.add(scroll);

	var main = Titanium.UI.createView({
		backgroundImage:'images/contato/bg.jpg',top:0,left:0,width:320*res,height:1460.5*res
	});
	scroll.add(main);

	win.add(headerTitle);

	return win;
}
module.exports = contato;