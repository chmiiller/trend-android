function legenda() {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;

	var win = Titanium.UI.createWindow({
		backgroundColor:'#243649',
		navBarHidden: true,
		fullscreen: true,
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });
	
	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor:'white',top:0,left:0,width:320*rw,height:70*rh
	});
	win.add(header);

	var headerTitle = Titanium.UI.createLabel({
		text:'Legendas e Termos',top:16*rh,
		width:320*rw,height:46.5*rh,color:'#4783c1',
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
		font:{fontSize:20*rw}
	});

	var scroll = Titanium.UI.createScrollView({
		top:71*rh,left:0,width:Ti.UI.FILL,height:'auto'
	});
	win.add(scroll);

	var main = Titanium.UI.createView({
		backgroundImage:'images/legendas/legendas.png',top:0,left:0,width:320*rw,height:852*rh
	});
	scroll.add(main);

	win.add(headerTitle);

	return win;
}
module.exports = legenda;