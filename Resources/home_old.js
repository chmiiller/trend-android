function home() {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	Titanium.App.clicouCorporativo = false;

	var win = Titanium.UI.createWindow({
		navBarHidden: true,
		fullscreen: true,
		backgroundColor:'#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundImage:'images/dashboard/bgHeader.jpg',top:0,width:320*rw,height:65*rh
	});

	win.add(header);

	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSobre.png',top:26*rh,left:160*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSobre);

	btSobre.addEventListener('click',function(e){
		var sobreWindow = require('sobre');
		var sobre = sobreWindow();
		sobre.open();
		//Ti.App.navigation.openWindow(sobre, {animated:true});
	});

	var btContato = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btContato.png',top:26*rh,left:206*rw,width:30.5*res,height:30.5*res
	});
	header.add(btContato);

	btContato.addEventListener('click',function(e){
		var contatoWindow = require('contato');
		var contato = contatoWindow();
		contato.open();
	});
	
	var btSearch = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSearch.png',top:26*rh,left:250*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSearch);

	btSearch.addEventListener('click',function(e){
		var filtroWindow = require('filtro');
		var filtro = filtroWindow();
		filtro.open();
	});
	
	//MAIN SCROLL
	var scroll = Titanium.UI.createScrollView({
		width:320*rw,height:'auto',
		top:header.getTop() + header.getHeight(),
		backgroundColor:'#f6f6f6',
		layout:'vertical',
	});
	win.add(scroll);

	//ROWS
	var rowCorporativo = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btCorporativo01.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'corporativo'
	});
	scroll.add(rowCorporativo);

	var rowCorporativo2 = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btCorporativo02.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'corporativo2'
	});
	scroll.add(rowCorporativo2);

	var rowCorporativo3 = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btCorporativo03.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'corporativo3'
	});
	scroll.add(rowCorporativo3);

	var rowCorporativo4 = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btCorporativo04.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'corporativo4'
	});
	scroll.add(rowCorporativo4);

	var rowInternacional = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btInternacional.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'internacional'
	});
	scroll.add(rowInternacional);

	var rowLazer = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btLazer.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'lazer'
	});
	scroll.add(rowLazer);

	var rowEventos = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btEventos.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'eventos'
	});
	scroll.add(rowEventos);

	var rowFeiras = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btFeiras.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'feiras'
	});
	scroll.add(rowFeiras);

	var rowCentral = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btCentralCarro.jpg',
		top:0,left:0,width:320*res,height:128*res,data:'central'
	});
	scroll.add(rowCentral);

	var rowReceptivo = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btReceptivo.jpg',
		top:0,left:0,width:320*res,height:130*res,data:'receptivo'
	});
	scroll.add(rowReceptivo);

	var rowTarifario = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btTarifario.png',
		top:0,left:0,width:320*res,height:130*res,data:'tarifario'
	});
	scroll.add(rowTarifario);

	rowCorporativo.addEventListener('click',rowHandler);
	rowCorporativo2.addEventListener('click',rowHandler);
	rowCorporativo3.addEventListener('click',rowHandler);
	rowCorporativo4.addEventListener('click',rowHandler);
	rowInternacional.addEventListener('click',rowHandler);
	rowLazer.addEventListener('click',rowHandler);
	rowEventos.addEventListener('click',rowHandler);
	rowFeiras.addEventListener('click',rowHandler);
	rowCentral.addEventListener('click',rowHandler);
	rowReceptivo.addEventListener('click',rowHandler);
	rowTarifario.addEventListener('click',rowHandler);

	function rowHandler(e){
		console.log(e.source.data);
		switch(e.source.data){
			case 'corporativo':
				if(!Titanium.App.clicouCorporativo){
					Titanium.App.clicouCorporativo = true;
					var listaWindow = require('lista');
					var lista = listaWindow(e.source.data);
					lista.open();
					
					/*var testeWindow = require('teste');
					var teste = testeWindow();
					teste.open();*/
				}
			break;

			case 'corporativo2':
				if(!Titanium.App.clicouCorporativo){
					Titanium.App.clicouCorporativo = true;
					var listaWindow = require('lista');
					var lista = listaWindow(e.source.data);
					lista.open();
				}
			break;

			case 'corporativo3':
				if(!Titanium.App.clicouCorporativo){
					Titanium.App.clicouCorporativo = true;
					var listaWindow = require('lista');
					var lista = listaWindow(e.source.data);
					lista.open();
				}
			break;

			case 'corporativo4':
				if(!Titanium.App.clicouCorporativo){
					Titanium.App.clicouCorporativo = true;
					var listaWindow = require('lista');
					var lista = listaWindow(e.source.data);
					lista.open();
				}
			break;

			case 'internacional':
				var listaWindow = require('lista');
				var lista = listaWindow(e.source.data);
				lista.open();
			break;

			case 'lazer':
				var listaWindow = require('lista');
				var lista = listaWindow(e.source.data);
				lista.open();
			break;

			case 'eventos':
				var listaWindow = require('lista');
				var lista = listaWindow(e.source.data);
				lista.open();
			break;

			case 'feiras':
				var feirasWindow = require('feiras');
				var feiras = feirasWindow();
				feiras.open();
			break;

			case 'central':
				var centralCarroWindow = require('centralCarro');
				var centralCarro = centralCarroWindow();
				centralCarro.open();
			break;

			case 'receptivo':
				openPDF('receptivo.pdf');
			break;

			case 'tarifario':
				openPDF('tarifario.pdf');
			break;
		}
		
	}

	function openPDF(filename) {
	   var tmpdir = Ti.Filesystem.getFile(Ti.Filesystem.tempDirectory,'pdfs');
	   tmpdir.createDirectory();
	   try {
		   var tmpFile = Ti.Filesystem.getFile(tmpdir.nativePath, filename ); 
		   var f = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, filename);
		   tmpFile.write(f.read());
		   Ti.Android.currentActivity.startActivity(
		   		Ti.Android.createIntent({
	                action: Ti.Android.ACTION_VIEW,
	                type: 'application/pdf',
	                data: tmpFile.getNativePath()
	            })
	       );
		}
		catch (err) {
		    var alertDialog = Titanium.UI.createAlertDialog({
		        title: 'Sem App para abrir arquivos PDF',
		        //message: 'We tried to open a PDF but failed. Do you want to search the marketplace for a PDF viewer?',
		        message: 'Tentamos abrir um arquivo PDF, mas algo deu errado. Deseja procurar por algum app na loja online?',
		        buttonNames: ['Yes','No'],
		        cancel: 1
		    });
		    alertDialog.show();
		    alertDialog.addEventListener('click', function(evt) {
		        if (evt.index == 0) {
		            Ti.Platform.openURL('http://search?q=pdf');
		        }
		    });
		}
	}

	return win;
}

module.exports = home;