function gradeObj(index,callback,data) {

	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;

	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var dataEstados = 
	[
		{title:'AC',valor:'Acre'},{title:'AL',valor:'Alagoas'},
		{title:'AP',valor:'Amapá'},{title:'AM',valor:'Amazonas'},
		{title:'BA',valor:'Bahia'},{title:'CE',valor:'Ceará'},
		{title:'DF',valor:'Distrito Federal'},{title:'ES',valor:'Espírito Santo'},
		{title:'GO',valor:'Goiás'},{title:'MA',valor:'Maranhão'},
		{title:'MT',valor:'Mato Grosso'},{title:'MS',valor:'Mato Grosso do Sul'},
		{title:'MG',valor:'Minas Gerais'},{title:'PR',valor:'Paraná'},
		{title:'PB',valor:'Paraíba'},{title:'PA',valor:'Pará'},
		{title:'PE',valor:'Pernambuco'},{title:'PI',valor:'Piauí'},
		{title:'RJ',valor:'Rio de Janeiro'},{title:'RN',valor:'Rio Grande do Norte'},
		{title:'RS',valor:'Rio Grande do Sul'},{title:'RO',valor:'Rondônia'},
		{title:'RR',valor:'Roraima'},{title:'SC',valor:'Santa Catarina'},
		{title:'SE',valor:'Sergipe'},{title:'SP',valor:'São Paulo'},
		{title:'TO',valor:'Tocantins'}
	];

	fakeData = {
		image:'http://www.hotelschool.co.za/files/2011/12/Hotel-Resort.jpg',
		title:'Hotel Resort',
		starsCount:3
	};

	var mainImage = fakeData.image;
	var id = data.Id;
	var title = data.Nome;
	var estado = abreviarEstado(data.Cidade.Estado.Nome);
	var cidade = data.Cidade.Nome + ' / ' + estado;
	var starsCount = data.Estrelas;
	var novaStrFoto = data.Foto.replace(/ /gi, "%20");
	var imageUrl = Titanium.App.Properties.mainUrl + novaStrFoto;

	function abreviarEstado(nomeEstado){
		var abrev = '';
		for(var i = 0; i < dataEstados.length; i++){
			var _item = dataEstados[i];
			if(nomeEstado == _item.valor){
				abrev = _item.title;
			}
		}
		return abrev;
	}

	var win = Titanium.UI.createView({
		backgroundImage:'images/lista/hotelContainerGrade.png',
		top:5*rh,left:5*rw,
		width:122*res,height:139.5*res
	});

	switch(index){
		case 0:
			win.setTop(5*rh);
			win.setLeft(25*rw);
		break;

		case 1:
			win.setTop(5*rh);
			win.setLeft(175*rw);
		break;

		case 2:
			win.setTop(150*rh);
			win.setLeft(25*rw);
		break;

		case 3:
			win.setTop(150*rh);
			win.setLeft(175*rw);
		break;
	}

	var imageView = Titanium.UI.createImageView({
		image:imageUrl,top:3*rh,width:148*res,height:93*res,
		defaultImage:'images/defaultImageGrade.jpg'
	});
	win.add(imageView);

	//CITY
	var cidadeContainer = Titanium.UI.createView({
		backgroundImage:'images/lista/boxCidade.png',
		top:12*rh,right:0,
		width:113.5*res,height:25*res,opacity:0.8
	});
	var lbCidade = Titanium.UI.createLabel({
		text:cidade,
		color:'#4e4e4e',
		width:110*res,height:20*res,
		right:1*rw,
		font:{fontSize:9*res},
		textAlign:Ti.UI.TEXT_ALIGNMENT_RIGHT
	});
	cidadeContainer.add(lbCidade);
	win.add(cidadeContainer);

	var reflexo = Titanium.UI.createView({
		backgroundImage:'images/lista/reflexoGrade.png',top:3*rh,width:148*res,height:123*res
	});

	win.add(reflexo);

	var lbTitle = Titanium.UI.createLabel({
		text:title,top:93*rh,left:0,width:121*res,height:14*res,
		color:'#4e4e4e',font:{fontSize:10*rw},
		textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
	});
	win.add(lbTitle);

	var starsContainer = Titanium.UI.createView({
		backgroundColor:'transparent',top:113*rh,left:30*rw,width:65*res,height:15*res,
	});
	var starAlignCenter = Titanium.UI.createView({
		backgroundColor:'transparent',layout:'horizontal',width:Ti.UI.SIZE,height:Ti.UI.SIZE,
		horizontalWrap:false
	});
	win.add(starsContainer);
	starsContainer.add(starAlignCenter);
	populateStarsNumber(starsCount);

	win.addEventListener('click',function(e){
		callback(id);
	});

	function populateStarsNumber(starsCount){
		for(var i = 0; i < starsCount; i++){
			var star = Titanium.UI.createView({
				backgroundImage:'images/lista/estrelaGrade.png',left:3*rw,width:9.5*res,height:9*res
			});
			starAlignCenter.add(star);
		}
	}

	return win;
}

module.exports = gradeObj;