function centralCarroInfos(nome) {
	var rw = Titanium.App.rw;
	var rh = Titanium.App.rh;
	var res;
	if(rw < rh){
		res = rw;
	}else{
		res = rh;
	}

	var win = Titanium.UI.createWindow({
		navBarHidden : true,
		fullscreen : true,
		backgroundColor : '#f6f6f6',
		orientationModes:[Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]
	});

	Ti.Gesture.addEventListener('orientationchange', function(e) {
        Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
    });

	var brieFont = 'Brie Light';
	var yanoneBold = 'Yanone Kaffeesatz';

	var mainArray = [];
	var mainArrayLength = 0;

	var file = null;
	var blob = null;
	var readText = null;
	var data = null;


	file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, "trend.txt");
	if(file.exists()){
		blob = file.read();
		if(blob){
			readText = blob.text;
			data = JSON.parse(readText);
			mainArray = data.Locadora;
			mainArrayLength = mainArray.length;
			Titanium.App.Properties.currentSession = 'central';

			file = null;
			blob = null;
			readText = null;
		}
	}

	var locadora;
	for(var i = 0; i < mainArrayLength; i++){
		var _item = mainArray[i];
		if(_item.Nome == nome){
			locadora = _item;
			console.log(locadora);
		}
	}

	//HEADER AND HEADER BUTTONS
	var header = Titanium.UI.createView({
		backgroundColor : 'white',
		top : 0,
		left : 0,
		width : 320*res,
		height : 65*res
	});

	win.add(header);

	var logoTrend = Titanium.UI.createView({
		backgroundImage:'images/trendLogo.png',top:28*rh,left:40*rw,width:79*res,height:32*res
	});
	header.add(logoTrend);
	
	var btSobre = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSobre.png',top:26*rh,left:160*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSobre);
	btSobre.addEventListener('click', function(e) {
		var sobreWindow = require('sobre');
		var sobre = sobreWindow();
		sobre.open();
	});

	var btContato = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btContato.png',top:26*rh,left:206*rw,width:30.5*res,height:30.5*res
	});
	header.add(btContato);

	btContato.addEventListener('click',function(e){
		var contatoWindow = require('contato');
		var contato = contatoWindow();
		contato.open();
	});

	var btSearch = Titanium.UI.createButton({
		backgroundImage:'images/dashboard/btSearch.png',top:26*rh,left:250*rw,width:30.5*res,height:30.5*res
	});
	header.add(btSearch);
	btSearch.addEventListener('click', function(e) {
		var filtroWindow = require('filtro');
		var filtro = filtroWindow();
		filtro.open();
	});

	//TITLE
	var title = Titanium.UI.createView({
		backgroundImage : 'images/central/tituloCentraldoCarro.png',
		top : 66*rh,
		left : 0,
		width : 320*res,
		height : 50.5*res
	});
	win.add(title);

	var base = Ti.UI.createScrollView({
		top : 117*rh,
		width : 320*res,
		height : Ti.UI.FILL,
	});

	win.add(base);

	var headerEmpresa = Ti.UI.createImageView({
		top : 0,
		width : 320*res,
		height : 50*res,
	});
	base.add(headerEmpresa);

	switch(nome){
		case 'Avis':
			headerEmpresa.image = 'images/central/headerAvis.png';
		break;

		case 'Hertz':
			headerEmpresa.image = 'images/central/headerHertz.png';
		break;

		case 'Movida':
			headerEmpresa.image = 'images/central/headerMovida.png';
		break;

		case 'Unidas':
			headerEmpresa.image = 'images/central/headerUnidas.png';
		break;
	}

	//HEADER INFOS
	var informacaoHeaderContainer = Ti.UI.createView({
		top : 52*rh,
		width : 320*res,
		height : 75*res,
		backgroundColor:'white'
	});

	var informacosHeaderImage = Ti.UI.createView({
		backgroundImage : 'images/central/rowInformacoes.png',
		left:-5*rw,
		top:0,
		width:320*res,
		height:75*res
	});
	informacaoHeaderContainer.add(informacosHeaderImage);

	var btnFecha = Ti.UI.createImageView({
		width : 25*res,
		height : 25*res,
		right : 15*rw,
		backgroundImage : 'images/precos/btFechar.png',
	});

	informacaoHeaderContainer.add(btnFecha);

	base.add(informacaoHeaderContainer);
	//areaInfos

	var informacoesContainer = Ti.UI.createView({
		top : 123*rh,
		width : Ti.UI.FILL,
		height : 180*res,
		backgroundImage : 'images/central/contentBg.png',
	});
	base.add(informacoesContainer);


	var texto3 = '<font color="white" face="helvetica" size="2">' + locadora.Condicoes.Descricao + ' <br><br> ' + locadora.TarifasIncluems.Descricao + '<br><br> '+ locadora.TarifasNaoIncluem.Descricao + '</font>';
	var txtSobre = Titanium.UI.createWebView({
		top:10*rh,left:0,width:320*res,height:170*res,
		backgroundColor:'transparent',
		html:texto3,
		softKeyboardOnFocus: Titanium.UI.Android.SOFT_KEYBOARD_HIDE_ON_FOCUS,
		enableZoomControls:false
	});
	informacoesContainer.add(txtSobre);

	////
	var tabelaContainer = Ti.UI.createScrollView({
		top : 320*rh,
		width : 320*res,
		scrollType : 'horizontal',
		height : 242*res,
		backgroundColor : '#6a4838'
	});

	var tableHeader = Ti.UI.createImageView({
		width : 600*res,
		height : 43*res,
		top : 0,
		left : 0,
		backgroundImage : 'images/central/tableHeader.png',
	});
	tabelaContainer.add(tableHeader);

	var posY = 61*res;
	var tableSize = locadora.Carro.length;
	var tableContainerHeight = (60*res) * (tableSize+1);
	tabelaContainer.setHeight(tableContainerHeight);

	for(var i = 0; i < tableSize; i++){
		var _item = locadora.Carro[i];
		var tableRow = Ti.UI.createView({
			width:600*res, height:60*res, top:posY, left:0,
			backgroundImage:'images/central/tableRow.png',
		});
		posY += 60*res;

		var lbGrupo = Titanium.UI.createLabel({
			text:_item.Grupo,left:13*rw,height:30*res,width:50*res,color:'black',
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER,
			font:{fontSize:12*res},
		});
		tableRow.add(lbGrupo);

		var lbModelo = Titanium.UI.createLabel({
			text:_item.Modelo,left:75*rw,height:30*res,width:240*res,color:'black',
			font:{fontSize:11*res},
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
		});
		tableRow.add(lbModelo);

		var lbDiaria = Titanium.UI.createLabel({
			text:'R$' + _item.Diaria,left:328*rw,height:30*res,width:85*res,color:'black',
			font:{fontSize:11*res},
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
		});
		tableRow.add(lbDiaria);

		var lbSemana = Titanium.UI.createLabel({
			text:'R$' + _item.Semana,left:418*rw,width:85*res,color:'black',
			font:{fontSize:11*res},
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
		});
		tableRow.add(lbSemana);

		var lbMes = Titanium.UI.createLabel({
			text:'R$' + _item.Mes,left:510*rw,width:85*res,color:'black',
			font:{fontSize:11*res},
			textAlign:Ti.UI.TEXT_ALIGNMENT_CENTER
		});
		tableRow.add(lbMes);

		tabelaContainer.add(tableRow);
	}

	base.add(tabelaContainer);

	return win;
}

module.exports = centralCarroInfos;
